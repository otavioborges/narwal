# Narwal - The aquatic TCP/IP stack

Narwal is a small footprint TCP/IP stack that can be used in either Linux or embedded environments.

The library compiles in a static library and all options and configurations are set on the *narwal_opt.h* header file. The example provided on the repository has implementation to work with pthreads or FreeRTOS.

## Compiling

Simply run **make** on the root of the project. If you wish to compile for embedded applications replace the *gcc* commands on the *Makefile* to point to the crosschain GCC compiler, comment the binary **TARGET** definition and remove the comment for the library **TARGET**, Makefile should like something like this:

```
TARGET=$(BINDIR)/libnarwal.a
%TARGET=$(BINDIR)/narwal
(...)
$(TARGET): $(OBJFILES)
	/your/toolchain/arm-none-gcc $(CFLAGS) -o $@ $^ $(LDFLAGS)
```

Copy the headers from *inc* folder and *libnarwal.a* files to your project and add the following flag on the linker command:

```
$ gcc -o yourProject -lnarwal -c source.c
```
