/*
 * tcp.h
 *
 *  Created on: Dec. 29, 2020
 *      Author: root
 */

#ifndef INC_TCP_H_
#define INC_TCP_H_

#define NARWAL_PACKET_TCP_LENGTH     20

#include "narwal.h"


typedef enum{
	TCP_OPTION_EOO       = (uint8_t)0,
	TCP_OPTION_NOP       = (uint8_t)1,
	TCP_OPTION_SEG_SIZE  = (uint8_t)2,
	TCP_OPTION_WINDOW    = (uint8_t)3,
	TCP_OPTION_SACK_PERM = (uint8_t)4,
	TCP_OPTION_SACK      = (uint8_t)5,
	TCP_OPTION_TIMESTAMP = (uint8_t)8
}tcp_option_value_t;

typedef enum{
	TCP_FLAG_FIN         = (uint16_t)0x001,
	TCP_FLAG_SYN         = (uint16_t)0x002,
	TCP_FLAG_RST         = (uint16_t)0x004,
	TCP_FLAG_PSH         = (uint16_t)0x008,
	TCP_FLAG_ACK         = (uint16_t)0x010,
	TCP_FLAG_URG         = (uint16_t)0x020,
	TCP_FLAG_ECE         = (uint16_t)0x040,
	TCP_FLAG_CWR         = (uint16_t)0x080,
	TCP_FLAG_NS          = (uint16_t)0x100
}tcp_flags_t;

typedef struct{
	uint16_t sourcePort;
	uint16_t destPort;
	uint32_t sequenceNumber;
	uint32_t ackNumber;
	uint8_t dataOffsetNS;
	uint8_t flags;
	uint16_t windowSize;
	uint16_t checksum;
	uint16_t urgentPointer;
}tcp_header_t;

typedef struct{
	uint8_t option;
	uint8_t length;
	uint8_t value;
}tcp_option_t;

void NARWAL_tcp_parsePackage(uint8_t *buffer, size_t length, struct narwal_addr sender);

#endif /* INC_TCP_H_ */
