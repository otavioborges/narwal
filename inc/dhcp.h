/*
/* dhcp.h
/*
/*  Created on: Dec. 29, 2020
/*      Author: root
*/

#ifndef INC_DHCP_H_
#define INC_DHCP_H_

#define NARWAL_PACKET_DHCP_LENGTH     240
#define NARWAL_DHCP_MAGIC_COOKIE      (uint32_t)0x63825363U

#include "narwal.h"
#include <stdint.h>

typedef struct{
	uint8_t opcode;
	uint8_t hwType;
	uint8_t hwAddrLength;
	uint8_t hopCount;
	uint32_t transactionID;
	uint16_t seconds;
	uint16_t flags;
	uint32_t clientAddr;
	uint32_t yourAddr;
	uint32_t serverAddr;
	uint32_t gatewayAddr;
	uint8_t clientHw[16];
	uint8_t serverName[64];
	uint8_t boot[128];
	uint32_t magicCookie;
}dhcp_header_t;

typedef struct{
	uint8_t code;
	uint8_t length;
	uint8_t value;
}dhcp_option_t;

typedef enum{
	DHCP_STATE_DISCOVER = (uint8_t)0x01,
	DHCP_STATE_OFFER    = (uint8_t)0x02,
	DHCP_STATE_BOUND    = (uint8_t)0x04,
	DHCP_STATE_RENEWING = (uint8_t)0x08
}dhcp_state_t;

typedef enum{
	DHCP_OPCODE_REQUEST = (uint8_t)0x01,
	DHCP_OPCODE_REPLY   = (uint8_t)0x02
}dhcp_opcode_t;

typedef enum{
	DHCP_OPTION_PAD					                        =(uint8_t)0U,
	DHCP_OPTION_SUBNET_MASK			                        =(uint8_t)1U,
	DHCP_OPTION_TIME_OFFSET			                        =(uint8_t)2U,
	DHCP_OPTION_ROUTER				                        =(uint8_t)3U,
	DHCP_OPTION_TIME_SERVER			                        =(uint8_t)4U,
	DHCP_OPTION_NAME_SERVER			                        =(uint8_t)5U,
	DHCP_OPTION_DOMAIN_SERVER		                        =(uint8_t)6U,
	DHCP_OPTION_LOG_SERVER			                        =(uint8_t)7U,
	DHCP_OPTION_QUOTES_SERVER		                        =(uint8_t)8U,
	DHCP_OPTION_LPR_SERVER			                        =(uint8_t)9U,
	DHCP_OPTION_IMPRESS_SERVER		                        =(uint8_t)10U,
	DHCP_OPTION_RLP_SERVER			                        =(uint8_t)11U,
	DHCP_OPTION_HOSTNAME			                        =(uint8_t)12U,
	DHCP_OPTION_BOOT_FILE_SIZE		                        =(uint8_t)13U,
	DHCP_OPTION_MERIT_DUMP_FILE		                        =(uint8_t)14U,
	DHCP_OPTION_DOMAIN_NAME			                        =(uint8_t)15U,
	DHCP_OPTION_SWAP_SERVER			                        =(uint8_t)16U,
	DHCP_OPTION_ROOT_PATH			                        =(uint8_t)17U,
	DHCP_OPTION_EXTENSION_FILE		                        =(uint8_t)18U,
	DHCP_OPTION_FORWARD_ON_OFF		                        =(uint8_t)19U,
	DHCP_OPTION_SRCRTE_ON_OFF		                        =(uint8_t)20U,
	DHCP_OPTION_POLICY_FILTER		                        =(uint8_t)21U,
	DHCP_OPTION_MAX_DG_ASSEMBLY		                        =(uint8_t)22U,
	DHCP_OPTION_DEFAULT_IP_TTL		                        =(uint8_t)23U,
	DHCP_OPTION_MTU_TIMEOUT			                        =(uint8_t)24U,
	DHCP_OPTION_MTU_PLATEAU			                        =(uint8_t)25U,
	DHCP_OPTION_MTU_INTERFACE		                        =(uint8_t)26U,
	DHCP_OPTION_MTU_SUBNET			                        =(uint8_t)27U,
	DHCP_OPTION_BROADCAST_ADDRESS	                        =(uint8_t)28U,
	DHCP_OPTION_MASK_DISCOVERY		                        =(uint8_t)29U,
	DHCP_OPTION_MASK_SUPPLIER		                        =(uint8_t)30U,
	DHCP_OPTION_ROUTER_DISCOVERY	                        =(uint8_t)31U,
	DHCP_OPTION_ROUTER_REQUEST		                        =(uint8_t)32U,
	DHCP_OPTION_STATIC_ROUTE		                        =(uint8_t)33U,
	DHCP_OPTION_TRAILERS			                        =(uint8_t)34U,
	DHCP_OPTION_ARP_TIMEOUT			                        =(uint8_t)35U,
	DHCP_OPTION_ETHERNET			                        =(uint8_t)36U,
	DHCP_OPTION_DEFAULT_TCP_TTL		                        =(uint8_t)37U,
	DHCP_OPTION_KEEPALIVE_TIME		                        =(uint8_t)38U,
	DHCP_OPTION_KEEPALIVE_DATA		                        =(uint8_t)39U,
	DHCP_OPTION_NIS_DOMAIN			                        =(uint8_t)40U,
	DHCP_OPTION_NIS_SERVERS			                        =(uint8_t)41U,
	DHCP_OPTION_NTP_SERVERS			                        =(uint8_t)42U,
	DHCP_OPTION_VENDOR_SPECIFIC		                        =(uint8_t)43U,
	DHCP_OPTION_NETBIOS_NAME_SRV	                        =(uint8_t)44U,
	DHCP_OPTION_NETBIOS_DIST_SRV	                        =(uint8_t)45U,
	DHCP_OPTION_NETBIOS_NODE_TYPE	                        =(uint8_t)46U,
	DHCP_OPTION_NETBIOS_SCOPE		                        =(uint8_t)47U,
	DHCP_OPTION_X_WINDOW_FONT		                        =(uint8_t)48U,
	DHCP_OPTION_X_WINDOW_MANAGER	                        =(uint8_t)49U,
	DHCP_OPTION_ADDRESS_REQUEST		                        =(uint8_t)50U,
	DHCP_OPTION_ADDRESS_TIME		                        =(uint8_t)51U,
	DHCP_OPTION_OVERLOAD			                        =(uint8_t)52U,
	DHCP_OPTION_DHCP_MSG_TYPE		                        =(uint8_t)53U,
	DHCP_OPTION_DHCP_SERVER_ID		                        =(uint8_t)54U,
	DHCP_OPTION_PARAMETER_LIST		                        =(uint8_t)55U,
	DHCP_OPTION_DHCP_MESSAGE		                        =(uint8_t)56U,
	DHCP_OPTION_DHCP_MAX_MSG_SIZE	                        =(uint8_t)57U,
	DHCP_OPTION_RENEWAL_TIME		                        =(uint8_t)58U,
	DHCP_OPTION_REBINDING_TIME		                        =(uint8_t)59U,
	DHCP_OPTION_CLASS_ID			                        =(uint8_t)60U,
	DHCP_OPTION_CLIENT_ID			                        =(uint8_t)61U,
	DHCP_OPTION_NETWARE_IP_DOMAIN	                        =(uint8_t)62U,
	DHCP_OPTION_NETWARE_IP_OPTION	                        =(uint8_t)63U,
	DHCP_OPTION_NIS_DOMAIN_NAME		                        =(uint8_t)64U,
	DHCP_OPTION_NIS_SERVER_ADDR		                        =(uint8_t)65U,
	DHCP_OPTION_SERVER_NAME			                        =(uint8_t)66U,
	DHCP_OPTION_BOOTFILE_NAME		                        =(uint8_t)67U,
	DHCP_OPTION_HOME_AGENT_ADDRS	                        =(uint8_t)68U,
	DHCP_OPTION_SMTP_SERVER			                        =(uint8_t)69U,
	DHCP_OPTION_POP3_SERVER			                        =(uint8_t)70U,
	DHCP_OPTION_NNTP_SERVER			                        =(uint8_t)71U,
	DHCP_OPTION_WWW_SERVER			                        =(uint8_t)72U,
	DHCP_OPTION_FINGER_SERVER		                        =(uint8_t)73U,
	DHCP_OPTION_IRC_SERVER			                        =(uint8_t)74U,
	DHCP_OPTION_STREETTALK_SERVER	                        =(uint8_t)75U,
	DHCP_OPTION_STDA_SERVER			                        =(uint8_t)76U,
	DHCP_OPTION_USER_CLASS			                        =(uint8_t)77U,
	DHCP_OPTION_DIRECTORY_AGENT		                        =(uint8_t)78U,
	DHCP_OPTION_SERVICE_SCOPE		                        =(uint8_t)79U,
	DHCP_OPTION_RAPID_COMMIT		                        =(uint8_t)80U,
	DHCP_OPTION_CLIENT_FQDN			                        =(uint8_t)81U,
	DHCP_OPTION_RELAY_AGENT_INFORMATION						=(uint8_t)82U,
	DHCP_OPTION_ISNS										=(uint8_t)83U,
	DHCP_OPTION_NDS_SERVERS									=(uint8_t)85U,
	DHCP_OPTION_NDS_TREE_NAME								=(uint8_t)86U,
	DHCP_OPTION_NDS_CONTEXT									=(uint8_t)87U,
	DHCP_OPTION_BCMCS_CONTROLLER_DOMAIN_NAME_LIST			=(uint8_t)88U,
	DHCP_OPTION_BCMCS_CONTROLLER_IPV4_ADDRESS_OPTION		=(uint8_t)89U,
	DHCP_OPTION_AUTHENTICATION								=(uint8_t)90U,
	DHCP_OPTION_CLIENT_LAST_TRANSACTION_TIME_OPTION			=(uint8_t)91U,
	DHCP_OPTION_ASSOCIATED_IP_OPTION						=(uint8_t)92U,
	DHCP_OPTION_CLIENT_SYSTEM								=(uint8_t)93U,
	DHCP_OPTION_CLIENT_NDI									=(uint8_t)94U,
	DHCP_OPTION_LDAP										=(uint8_t)95U,
	DHCP_OPTION_UUID_GUID									=(uint8_t)97U,
	DHCP_OPTION_USER_AUTH									=(uint8_t)98U,
	DHCP_OPTION_GEOCONF_CIVIC								=(uint8_t)99U,
	DHCP_OPTION_PCODE										=(uint8_t)100U,
	DHCP_OPTION_TCODE										=(uint8_t)101U,
	DHCP_OPTION_IPV6_ONLY_PREFERRED							=(uint8_t)108U,
	DHCP_OPTION_OPTION_DHCP4O6_S46_SADDR					=(uint8_t)109U,
	DHCP_OPTION_NETINFO_ADDRESS								=(uint8_t)112U,
	DHCP_OPTION_NETINFO_TAG									=(uint8_t)113U,
	DHCP_OPTION_DHCP_CAPTIVE_PORTAL							=(uint8_t)114U,
	DHCP_OPTION_AUTO_CONFIG									=(uint8_t)116U,
	DHCP_OPTION_NAME_SERVICE_SEARCH							=(uint8_t)117U,
	DHCP_OPTION_SUBNET_SELECTION_OPTION						=(uint8_t)118U,
	DHCP_OPTION_DOMAIN_SEARCH								=(uint8_t)119U,
	DHCP_OPTION_SIP_SERVERS_DHCP_OPTION						=(uint8_t)120U,
	DHCP_OPTION_CLASSLESS_STATIC_ROUTE_OPTION				=(uint8_t)121U,
	DHCP_OPTION_CCC											=(uint8_t)122U,
	DHCP_OPTION_GEOCONF_OPTION								=(uint8_t)123U,
	DHCP_OPTION_V_I_VENDOR_CLASS							=(uint8_t)124U,
	DHCP_OPTION_V_I_VENDOR_SPECIFIC_INFORMATION				=(uint8_t)125U,
	DHCP_OPTION_DOCSIS_FULL_SECURITY						=(uint8_t)128U,
	DHCP_OPTION_CALL_SERVER_IP_ADDRESS						=(uint8_t)129U,
	DHCP_OPTION_HTTP_PROXY_FOR_PHONE_SPECIFIC_APPLICATIONS	=(uint8_t)135U,
	DHCP_OPTION_OPTION_PANA_AGENT							=(uint8_t)136U,
	DHCP_OPTION_OPTION_V4_LOST								=(uint8_t)137U,
	DHCP_OPTION_OPTION_CAPWAP_AC_V4							=(uint8_t)138U,
	DHCP_OPTION_OPTION_IPV4_ADDRESS_MOS						=(uint8_t)139U,
	DHCP_OPTION_OPTION_IPV4_FQDN_MOS						=(uint8_t)140U,
	DHCP_OPTION_SIP_UA_CONFIGURATION_SERVICE_DOMAINS		=(uint8_t)141U,
	DHCP_OPTION_OPTION_IPV4_ADDRESS_ANDSF					=(uint8_t)142U,
	DHCP_OPTION_OPTION_V4_SZTP_REDIRECT						=(uint8_t)143U,
	DHCP_OPTION_GEOLOC										=(uint8_t)144U,
	DHCP_OPTION_FORCERENEW_NONCE_CAPABLE					=(uint8_t)145U,
	DHCP_OPTION_RDNSS_SELECTION								=(uint8_t)146U,
	DHCP_OPTION_OPTION_V4_DOTS_RI							=(uint8_t)147U,
	DHCP_OPTION_OPTION_V4_DOTS_ADDRESS						=(uint8_t)148U,
	DHCP_OPTION_TFTP_SERVER_ADDRESS							=(uint8_t)150U,
	DHCP_OPTION_GRUB_CONFIGURATION_PATH_NAME				=(uint8_t)150U,
	DHCP_OPTION_STATUS_CODE									=(uint8_t)151U,
	DHCP_OPTION_BASE_TIME									=(uint8_t)152U,
	DHCP_OPTION_START_TIME_OF_STATE							=(uint8_t)153U,
	DHCP_OPTION_QUERY_START_TIME							=(uint8_t)154U,
	DHCP_OPTION_QUERY_END_TIME								=(uint8_t)155U,
	DHCP_OPTION_DHCP_STATE									=(uint8_t)156U,
	DHCP_OPTION_DATA_SOURCE									=(uint8_t)157U,
	DHCP_OPTION_OPTION_V4_PCP_SERVER						=(uint8_t)158U,
	DHCP_OPTION_OPTION_V4_PORTPARAMS						=(uint8_t)159U,
	DHCP_OPTION_OPTION_MUD_URL_V4							=(uint8_t)161U,
	DHCP_OPTION_ETHERBOOT									=(uint8_t)175U,
	DHCP_OPTION_IP_TELEPHONE								=(uint8_t)176U,
	DHCP_OPTION_ETHERBOOT2									=(uint8_t)177U,
	DHCP_OPTION_PXELINUX_MAGIC								=(uint8_t)208U,
	DHCP_OPTION_CONFIGURATION_FILE							=(uint8_t)209U,
	DHCP_OPTION_PATH_PREFIX									=(uint8_t)210U,
	DHCP_OPTION_REBOOT_TIME									=(uint8_t)211U,
	DHCP_OPTION_OPTION_6RD									=(uint8_t)212U,
	DHCP_OPTION_OPTION_V4_ACCESS_DOMAIN						=(uint8_t)213U,
	DHCP_OPTION_SUBNET_ALLOCATION_OPTION					=(uint8_t)220U,
	DHCP_OPTION_VIRTUAL_SUBNET_SELECTION_VSS				=(uint8_t)221U,
	DHCP_OPTION_END											=(uint8_t)255U
}dhcp_option_value_t;

typedef enum{
	DHCP_TYPE_DISCOVER			= (uint8_t)0x01,
	DHCP_TYPE_OFFER				= (uint8_t)0x02,
	DHCP_TYPE_REQUEST			= (uint8_t)0x03,
	DHCP_TYPE_DECLINE			= (uint8_t)0x04,
	DHCP_TYPE_ACK				= (uint8_t)0x05,
	DHCP_TYPE_NAK				= (uint8_t)0x06,
	DHCP_TYPE_RELEASE			= (uint8_t)0x07,
	DHCP_TYPE_INFORM			= (uint8_t)0x08,
	DHCP_TYPE_FORCERENEW		= (uint8_t)0x09,
	DHCP_TYPE_LEASEQUERY		= (uint8_t)0x0A,
	DHCP_TYPE_LEASEUNASSIGNED	= (uint8_t)0x0B,
	DHCP_TYPE_LEASEUNKNOWN		= (uint8_t)0x0C,
	DHCP_TYPE_LEASEACTIVE		= (uint8_t)0x0D,
	DHCP_TYPE_BULKLEASEQUERY	= (uint8_t)0x0E,
	DHCP_TYPE_LEASEQUERYDONE	= (uint8_t)0x0F,
	DHCP_TYPE_ACTIVELEASEQUERY	= (uint8_t)0x10,
	DHCP_TYPE_LEASEQUERYSTATUS	= (uint8_t)0x11,
	DHCP_TYPE_TLS				= (uint8_t)0x12
}dhcp_type_t;

int NARWAL_dhcp_start(void);
int NARWAL_dhcp_BuildMessage(uint8_t *buffer, dhcp_opcode_t opcode, uint32_t transactionID, uint8_t broadcast);
int NARWAL_dhcp_AddOption(uint8_t *buffer, dhcp_option_value_t option, uint8_t optLength, uint8_t *value);
int NARWAL_dhcp_isBound(void);

#endif /* INC_DHCP_H_ */
