/*
 * data_link.h
 *
 *  Created on: Dec. 28, 2020
 *      Author: root
 */

#ifndef INC_ETH_H_
#define INC_ETH_H_

#define NARWAL_PACKET_ETH_LENGTH     14

#define NARWAL_ETH_MAC_LENGTH        6
#define NARWAL_ETH_MAC_STR_LENGTH    18

#include "narwal.h"
#include <stdint.h>

typedef enum{
	NARWAL_ETHERTYPE_IPv4          = (uint16_t)0x0800,
	NARWAL_ETHERTYPE_ARP           = (uint16_t)0x0806,
	NARWAL_ETHERTYPE_WOL           = (uint16_t)0x0842,
	NARWAL_ETHERTYPE_AVTP          = (uint16_t)0x22F0,
	NARWAL_ETHERTYPE_RARP          = (uint16_t)0x22F3,
	NARWAL_ETHERTYPE_Ethertalk     = (uint16_t)0x22EA,
	NARWAL_ETHERTYPE_AARP          = (uint16_t)0x6002,
	NARWAL_ETHERTYPE_VLANtagged    = (uint16_t)0x6003,
	NARWAL_ETHERTYPE_SLPP          = (uint16_t)0x6004,
	NARWAL_ETHERTYPE_VLACP         = (uint16_t)0x8035,
	NARWAL_ETHERTYPE_IPX           = (uint16_t)0x809B,
	NARWAL_ETHERTYPE_QNX           = (uint16_t)0x80F3,
	NARWAL_ETHERTYPE_IPv6          = (uint16_t)0x8100,
	NARWAL_ETHERTYPE_EFC           = (uint16_t)0x8102,
	NARWAL_ETHERTYPE_LACP          = (uint16_t)0x8103
}eth_ethertype_t;

typedef struct{
	uint8_t destMAC[6];
	uint8_t sourceMAC[6];
	uint16_t etherType;
}eth_header_t;

extern const struct narwal_hwAddr NARWAL_ETH_BROADCAST;
extern const struct narwal_hwAddr NARWAL_ETH_NONE;

int NARWAL_eth_parsePackage(uint8_t *buffer, size_t length);
int NARWAL_eth_createHeader(uint8_t *buffer, const uint8_t *destAddr, const uint8_t *srcAddr, eth_ethertype_t etherType);

#endif /* INC_ETH_H_ */
