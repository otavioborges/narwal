/*
 * arp.h
 *
 *  Created on: Dec. 28, 2020
 *      Author: root
 */

#ifndef INC_ARP_H_
#define INC_ARP_H_

#include "narwal.h"

#define NARWAL_PACKET_ARP_LENGTH     8

#ifdef NARWAL_IPv4_ENABLED
#define NARWAL_ARP_ENTRY_LENGTH      (NARWAL_HW_ADDR_LENGTH + 4)
#else
#define NARWAL_ARP_ENTRY_LENGTH      (NARWAL_HW_ADDR_LENGTH + 16)
#endif

#include <stdint.h>

typedef struct{
	uint16_t hwType;
	uint16_t protoType;
	uint8_t hwAddrLength;
	uint8_t protoAddrLength;
	uint16_t opcode;
}arp_header_t;

typedef enum{
	NARWAL_HWTYPE_ETHERNET    = (uint16_t)0x0001,
	NARWAL_HWTYPE_IEEE802     = (uint16_t)0x0006,
	NARWAL_HWTYPE_ARCNET      = (uint16_t)0x0007,
	NARWAL_HWTYPE_FRAME_RELAY = (uint16_t)0x000F,
	NARWAL_HWTYPE_ATM         = (uint16_t)0x0010,
	NARWAL_HWTYPE_HDLC        = (uint16_t)0x0011,
	NARWAL_HWTYPE_FIBRE       = (uint16_t)0x0012,
	NARWAL_HWTYPE_ATM2        = (uint16_t)0x0013,
	NARWAL_HWTYPE_SERIAL_LINE = (uint16_t)0x0014
}arp_hwtype_t;

typedef enum{
	NARWAL_OPCODE_ARP_REQUEST   = (uint16_t)0x0001,
	NARWAL_OPCODE_ARP_REPLY     = (uint16_t)0x0002,
	NARWAL_OPCODE_RARP_REQUEST  = (uint16_t)0x0003,
	NARWAL_OPCODE_RARP_REPLY    = (uint16_t)0x0004,
	NARWAL_OPCODE_DRARP_REQUEST = (uint16_t)0x0005,
	NARWAL_OPCODE_DRARP_REPLY   = (uint16_t)0x0006,
	NARWAL_OPCODE_DRARP_ERROR   = (uint16_t)0x0007,
	NARWAL_OPCODE_InARP_REQUEST = (uint16_t)0x0008,
	NARWAL_OPCODE_InARP_REPLY   = (uint16_t)0x0009
}arp_opcode_t;

void NARWAL_arp_parsePackage(uint8_t *buffer, size_t length, struct narwal_addr sender);
int NARWAL_arp_createPackage(struct narwal_addr who, uint8_t *payload);

#endif /* INC_ARP_H_ */
