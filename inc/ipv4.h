/*
 * ipv4.h
 *
 *  Created on: Dec. 28, 2020
 *      Author: root
 */

#ifndef INC_IPV4_H_
#define INC_IPV4_H_

#define NARWAL_PACKET_IPv4_LENGTH     20
#define NARWAL_PACKET_IPv4_OPT_LENGTH 16

#define NARWAL_IPv4_VERSION           0x40

#include "narwal.h"
#include <stdint.h>

typedef struct{
	uint8_t versionIHL;
	uint8_t dscpECN;
	uint16_t totalLength;
	uint16_t ID;
	uint16_t fragmentOffset;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t checksum;
	uint32_t sourceIP;
	uint32_t destIP;
}ipv4_header_t;

typedef enum{
	NARWAL_IPv4_PROTOCOL_HOPOPT				= (uint8_t)0x00,
	NARWAL_IPv4_PROTOCOL_ICMP				= (uint8_t)0x01,
	NARWAL_IPv4_PROTOCOL_IGMP				= (uint8_t)0x02,
	NARWAL_IPv4_PROTOCOL_GGP				= (uint8_t)0x03,
	NARWAL_IPv4_PROTOCOL_IP_IN_IP			= (uint8_t)0x04,
	NARWAL_IPv4_PROTOCOL_ST				    = (uint8_t)0x05,
	NARWAL_IPv4_PROTOCOL_TCP				= (uint8_t)0x06,
	NARWAL_IPv4_PROTOCOL_CBT				= (uint8_t)0x07,
	NARWAL_IPv4_PROTOCOL_EGP				= (uint8_t)0x08,
	NARWAL_IPv4_PROTOCOL_IGP				= (uint8_t)0x09,
	NARWAL_IPv4_PROTOCOL_BBN_RCC_MON		= (uint8_t)0x0A,
	NARWAL_IPv4_PROTOCOL_NVP_II				= (uint8_t)0x0B,
	NARWAL_IPv4_PROTOCOL_PUP				= (uint8_t)0x0C,
	NARWAL_IPv4_PROTOCOL_ARGUS				= (uint8_t)0x0D,
	NARWAL_IPv4_PROTOCOL_EMCON				= (uint8_t)0x0E,
	NARWAL_IPv4_PROTOCOL_XNET				= (uint8_t)0x0F,
	NARWAL_IPv4_PROTOCOL_CHAOS				= (uint8_t)0x10,
	NARWAL_IPv4_PROTOCOL_UDP				= (uint8_t)0x11,
	NARWAL_IPv4_PROTOCOL_MUX				= (uint8_t)0x12,
	NARWAL_IPv4_PROTOCOL_DCN_MEAS			= (uint8_t)0x13,
	NARWAL_IPv4_PROTOCOL_HMP				= (uint8_t)0x14,
	NARWAL_IPv4_PROTOCOL_PRM				= (uint8_t)0x15,
	NARWAL_IPv4_PROTOCOL_XNS_IDP			= (uint8_t)0x16,
	NARWAL_IPv4_PROTOCOL_TRUNK_1			= (uint8_t)0x17,
	NARWAL_IPv4_PROTOCOL_TRUNK_2			= (uint8_t)0x18,
	NARWAL_IPv4_PROTOCOL_LEAF_1				= (uint8_t)0x19,
	NARWAL_IPv4_PROTOCOL_LEAF_2				= (uint8_t)0x1A,
	NARWAL_IPv4_PROTOCOL_RDP				= (uint8_t)0x1B,
	NARWAL_IPv4_PROTOCOL_IRTP				= (uint8_t)0x1C,
	NARWAL_IPv4_PROTOCOL_ISO_TP4			= (uint8_t)0x1D,
	NARWAL_IPv4_PROTOCOL_NETBLT				= (uint8_t)0x1E,
	NARWAL_IPv4_PROTOCOL_MFE_NSP			= (uint8_t)0x1F,
	NARWAL_IPv4_PROTOCOL_MERIT_INP			= (uint8_t)0x20,
	NARWAL_IPv4_PROTOCOL_DCCP				= (uint8_t)0x21,
	NARWAL_IPv4_PROTOCOL_3PC				= (uint8_t)0x22,
	NARWAL_IPv4_PROTOCOL_IDPR				= (uint8_t)0x23,
	NARWAL_IPv4_PROTOCOL_XTP				= (uint8_t)0x24,
	NARWAL_IPv4_PROTOCOL_DDP				= (uint8_t)0x25,
	NARWAL_IPv4_PROTOCOL_IDPR_CMTP			= (uint8_t)0x26,
	NARWAL_IPv4_PROTOCOL_TPPP				= (uint8_t)0x27,
	NARWAL_IPv4_PROTOCOL_IL					= (uint8_t)0x28,
	NARWAL_IPv4_PROTOCOL_IPV6				= (uint8_t)0x29,
	NARWAL_IPv4_PROTOCOL_SDRP				= (uint8_t)0x2A,
	NARWAL_IPv4_PROTOCOL_IPV6_ROUTE			= (uint8_t)0x2B,
	NARWAL_IPv4_PROTOCOL_IPV6_FRAG			= (uint8_t)0x2C,
	NARWAL_IPv4_PROTOCOL_IDRP				= (uint8_t)0x2D,
	NARWAL_IPv4_PROTOCOL_RSVP				= (uint8_t)0x2E,
	NARWAL_IPv4_PROTOCOL_GRES				= (uint8_t)0x2F,
	NARWAL_IPv4_PROTOCOL_DSR				= (uint8_t)0x30,
	NARWAL_IPv4_PROTOCOL_BNA				= (uint8_t)0x31,
	NARWAL_IPv4_PROTOCOL_ESP				= (uint8_t)0x32,
	NARWAL_IPv4_PROTOCOL_AH					= (uint8_t)0x33,
	NARWAL_IPv4_PROTOCOL_I_NLSP				= (uint8_t)0x34,
	NARWAL_IPv4_PROTOCOL_SWIPE				= (uint8_t)0x35,
	NARWAL_IPv4_PROTOCOL_NARP				= (uint8_t)0x36,
	NARWAL_IPv4_PROTOCOL_MOBILE				= (uint8_t)0x37,
	NARWAL_IPv4_PROTOCOL_TLSP				= (uint8_t)0x38,
	NARWAL_IPv4_PROTOCOL_SKIP				= (uint8_t)0x39,
	NARWAL_IPv4_PROTOCOL_IPV6_ICMP			= (uint8_t)0x3A,
	NARWAL_IPv4_PROTOCOL_IPV6_NONXT			= (uint8_t)0x3B,
	NARWAL_IPv4_PROTOCOL_IPV6_OPTS			= (uint8_t)0x3C,
	NARWAL_IPv4_PROTOCOL_INTERNAL			= (uint8_t)0x3D,
	NARWAL_IPv4_PROTOCOL_CFTP				= (uint8_t)0x3E,
	NARWAL_IPv4_PROTOCOL_LOCAL				= (uint8_t)0x3F,
	NARWAL_IPv4_PROTOCOL_SAT_EXPAK			= (uint8_t)0x40,
	NARWAL_IPv4_PROTOCOL_KRYPTOLAN			= (uint8_t)0x41,
	NARWAL_IPv4_PROTOCOL_RVD				= (uint8_t)0x42,
	NARWAL_IPv4_PROTOCOL_IPPC				= (uint8_t)0x43,
	NARWAL_IPv4_PROTOCOL_DISTRIBUTED		= (uint8_t)0x44,
	NARWAL_IPv4_PROTOCOL_SAT_MON			= (uint8_t)0x45,
	NARWAL_IPv4_PROTOCOL_VISA				= (uint8_t)0x46,
	NARWAL_IPv4_PROTOCOL_IPCU				= (uint8_t)0x47,
	NARWAL_IPv4_PROTOCOL_CPNX				= (uint8_t)0x48,
	NARWAL_IPv4_PROTOCOL_CPHB				= (uint8_t)0x49,
	NARWAL_IPv4_PROTOCOL_WSN				= (uint8_t)0x4A,
	NARWAL_IPv4_PROTOCOL_PVP				= (uint8_t)0x4B,
	NARWAL_IPv4_PROTOCOL_BR_SAT_MON			= (uint8_t)0x4C,
	NARWAL_IPv4_PROTOCOL_SUN_ND				= (uint8_t)0x4D,
	NARWAL_IPv4_PROTOCOL_WB_MON				= (uint8_t)0x4E,
	NARWAL_IPv4_PROTOCOL_WB_EXPAK			= (uint8_t)0x4F,
	NARWAL_IPv4_PROTOCOL_ISO_IP				= (uint8_t)0x50,
	NARWAL_IPv4_PROTOCOL_VMTP				= (uint8_t)0x51,
	NARWAL_IPv4_PROTOCOL_SECURE_VMTP		= (uint8_t)0x52,
	NARWAL_IPv4_PROTOCOL_VINES				= (uint8_t)0x53,
	NARWAL_IPv4_PROTOCOL_TTP				= (uint8_t)0x54,
	NARWAL_IPv4_PROTOCOL_IPTM				= (uint8_t)0x54,
	NARWAL_IPv4_PROTOCOL_NSFNET_IGP			= (uint8_t)0x55,
	NARWAL_IPv4_PROTOCOL_DGP				= (uint8_t)0x56,
	NARWAL_IPv4_PROTOCOL_TCF				= (uint8_t)0x57,
	NARWAL_IPv4_PROTOCOL_EIGRP				= (uint8_t)0x58,
	NARWAL_IPv4_PROTOCOL_OSPF				= (uint8_t)0x59,
	NARWAL_IPv4_PROTOCOL_SPRITE_RPC			= (uint8_t)0x5A,
	NARWAL_IPv4_PROTOCOL_LARP				= (uint8_t)0x5B,
	NARWAL_IPv4_PROTOCOL_MTP				= (uint8_t)0x5C,
	NARWAL_IPv4_PROTOCOL_AX_25				= (uint8_t)0x5D,
	NARWAL_IPv4_PROTOCOL_OS					= (uint8_t)0x5E,
	NARWAL_IPv4_PROTOCOL_MICP				= (uint8_t)0x5F,
	NARWAL_IPv4_PROTOCOL_SCC_SP				= (uint8_t)0x60,
	NARWAL_IPv4_PROTOCOL_ETHERIP			= (uint8_t)0x61,
	NARWAL_IPv4_PROTOCOL_ENCAP				= (uint8_t)0x62,
	NARWAL_IPv4_PROTOCOL_ENCRYPTION			= (uint8_t)0x63,
	NARWAL_IPv4_PROTOCOL_GMTP				= (uint8_t)0x64,
	NARWAL_IPv4_PROTOCOL_IFMP				= (uint8_t)0x65,
	NARWAL_IPv4_PROTOCOL_PNNI				= (uint8_t)0x66,
	NARWAL_IPv4_PROTOCOL_PIM				= (uint8_t)0x67,
	NARWAL_IPv4_PROTOCOL_ARIS				= (uint8_t)0x68,
	NARWAL_IPv4_PROTOCOL_SCPS				= (uint8_t)0x69,
	NARWAL_IPv4_PROTOCOL_QNX				= (uint8_t)0x6A,
	NARWAL_IPv4_PROTOCOL_A_N				= (uint8_t)0x6B,
	NARWAL_IPv4_PROTOCOL_IPCOMP				= (uint8_t)0x6C,
	NARWAL_IPv4_PROTOCOL_SNP				= (uint8_t)0x6D,
	NARWAL_IPv4_PROTOCOL_COMPAQ_PEER		= (uint8_t)0x6E,
	NARWAL_IPv4_PROTOCOL_IPX_IN_IP			= (uint8_t)0x6F,
	NARWAL_IPv4_PROTOCOL_VRRP				= (uint8_t)0x70,
	NARWAL_IPv4_PROTOCOL_PGM				= (uint8_t)0x71,
	NARWAL_IPv4_PROTOCOL_ZERO_HOP			= (uint8_t)0x72,
	NARWAL_IPv4_PROTOCOL_L2TP				= (uint8_t)0x73,
	NARWAL_IPv4_PROTOCOL_DDX				= (uint8_t)0x74,
	NARWAL_IPv4_PROTOCOL_IATP				= (uint8_t)0x75,
	NARWAL_IPv4_PROTOCOL_STP				= (uint8_t)0x76,
	NARWAL_IPv4_PROTOCOL_SRP				= (uint8_t)0x77,
	NARWAL_IPv4_PROTOCOL_UTI				= (uint8_t)0x78,
	NARWAL_IPv4_PROTOCOL_SMP				= (uint8_t)0x79,
	NARWAL_IPv4_PROTOCOL_SM					= (uint8_t)0x7A,
	NARWAL_IPv4_PROTOCOL_PTP				= (uint8_t)0x7B,
	NARWAL_IPv4_PROTOCOL_IS_ISOVERIPV4		= (uint8_t)0x7C,
	NARWAL_IPv4_PROTOCOL_FIRE				= (uint8_t)0x7D,
	NARWAL_IPv4_PROTOCOL_CRTP				= (uint8_t)0x7E,
	NARWAL_IPv4_PROTOCOL_CRUDP				= (uint8_t)0x7F,
	NARWAL_IPv4_PROTOCOL_SSCOPMCE			= (uint8_t)0x80,
	NARWAL_IPv4_PROTOCOL_IPLT				= (uint8_t)0x81,
	NARWAL_IPv4_PROTOCOL_SPS				= (uint8_t)0x82,
	NARWAL_IPv4_PROTOCOL_PIPE				= (uint8_t)0x83,
	NARWAL_IPv4_PROTOCOL_SCTP				= (uint8_t)0x84,
	NARWAL_IPv4_PROTOCOL_FC					= (uint8_t)0x85,
	NARWAL_IPv4_PROTOCOL_RSVP_E2E_IGNORE	= (uint8_t)0x86,
	NARWAL_IPv4_PROTOCOL_MOBILITYHEADER		= (uint8_t)0x87,
	NARWAL_IPv4_PROTOCOL_UDPLITE			= (uint8_t)0x88,
	NARWAL_IPv4_PROTOCOL_MPLS_IN_IP			= (uint8_t)0x89,
	NARWAL_IPv4_PROTOCOL_MANET				= (uint8_t)0x8A,
	NARWAL_IPv4_PROTOCOL_HIP				= (uint8_t)0x8B,
	NARWAL_IPv4_PROTOCOL_SHIM6				= (uint8_t)0x8C,
	NARWAL_IPv4_PROTOCOL_WESP				= (uint8_t)0x8D,
	NARWAL_IPv4_PROTOCOL_ROHC				= (uint8_t)0x8E,
	NARWAL_IPv4_PROTOCOL_ETHERNET			= (uint8_t)0x8F
}ipv4_protocol_t;

uint32_t NARWAL_atoh(char *addr);
void NARWAL_htoa(char *__s, struct narwal_sockaddr_in __a);
void NARWAL_ipv4_setIP(uint32_t addr);
void NARWAL_ipv4_setNetmask(uint32_t addr);
void NARWAL_ipv4_setGateway(uint32_t addr);
void NARWAL_ipv4_setDNSServers(uint32_t *addrs, uint32_t length);

uint32_t NARWAL_ipv4_getBroadcast(void);
uint32_t NARWAL_ipv4_getGateway(void);

uint32_t NARWAL_ipv4_ourAddr(void);
int NARWAL_ipv4_matchAddr(uint32_t a, uint32_t b);

void NARWAL_ipv4_parsePackage(uint8_t *buffer, size_t length, struct narwal_addr sender);
int NARWAL_ipv4_createHeader(uint8_t *buffer, struct narwal_addr who, uint8_t *payload, uint32_t payloadLength, uint32_t ID, uint8_t protocol);

#endif /* INC_IPV4_H_ */
