#ifndef _INC_NARWAL_H_
#define _INC_NARWAL_H_

#include <stdint.h>
#include "narwal_opt.h"

#define HTOL(x)                       (uint32_t)((((uint32_t)x << 24) & 0xFF000000) | (((uint32_t)x << 8) & 0x00FF0000) | (((uint32_t)x >> 8) & 0x0000FF00) | (((uint32_t)x >> 24) & 0x000000FF))
#define HTOS(x)                       (uint16_t)((((uint16_t)x << 8) & 0xFF00) | (((uint16_t)x >> 8) & 0x00FF))

#define NARWAL_ASSERT_RESULT(x)       if(x != NARWAL_ERROR_OK) return x

/* Error definitions */
#define NARWAL_ERROR_OK               0
#define NARWAL_ERROR_BAD_DRIVER       -1
#define NARWAL_ERROR_FULL_BUFFER      -2
#define NARWAL_ERROR_NOT_FOUND        -3
#define NARWAL_ERROR_INIT_FAILED      -4

// Default definitions
#ifndef NARWAL_PENDING_TASKS_LENGTH
#define NARWAL_PENDING_TASKS_LENGTH   10
#endif

#ifndef NARWAL_ARP_TABLE_LENGTH
#define NARWAL_ARP_TABLE_LENGTH       255
#endif

#ifndef NARWAL_TIMEOUT
#define NARWAL_TIMEOUT                500
#endif

#define NARWAL_SYSTEM_SOCKET_COUNT    1 // TODO: calculate this by enabled protocols

struct narwal_hwAddr{
	uint8_t w[NARWAL_HW_ADDR_LENGTH];
};

#if (defined(NARWAL_IPv4_ENABLED) && defined(NARWAL_IPv6_ENABLED))
#error "Either use version 4 or 6 for the IP, both is complicated"
#endif

#if (!defined(NARWAL_IPv4_ENABLED) && !defined(NARWAL_IPv6_ENABLED))
#error "You must choose at least one version of IP"
#endif

struct narwal_addr{
	struct narwal_hwAddr mac;
#ifdef NARWAL_IPv4_ENABLED
	uint32_t s_addr;
#else
	uint8_t s_addr[16];
#endif
};

struct narwal_sockaddr_in{
	uint16_t sin_port;
	struct narwal_addr sin_addr;
	uint8_t sin_family;
};

typedef uint32_t                             socklen_t;

typedef void(* narwal_osThreadCreate)        (narwalThreadHandler *, void *, void *, uint32_t, int *);
typedef void(* narwal_osThreadExit)          (narwalThreadHandler *);
typedef void(* narwal_osJoin)                (narwalThreadHandler *);
typedef void(* narwal_osSemaphoreCreate)     (narwalSemaphoreHandler *, int, int, int *);
typedef void(* narwal_osSemaphoreTake)       (narwalSemaphoreHandler *, uint32_t, int *);
typedef void(* narwal_osSemaphoreGive)       (narwalSemaphoreHandler *);
typedef int(* narwal_osSemaphoreCount)       (narwalSemaphoreHandler *);
typedef void(* narwal_osMutexCreate)         (narwalMutexHandler *, int *);
typedef void(* narwal_osMutexLock)           (narwalMutexHandler *);
typedef void(* narwal_osMutexUnlock)         (narwalMutexHandler *);
typedef uint32_t(* narwal_osGetCurrentTicks) (void);
typedef void(* narwal_freeData)              (void *);
typedef void(* narwal_copyMem)               (void *, const void *, uint32_t);
typedef uint32_t(* narwal_getRandom)         (void);
typedef void(* narwal_delay)                 (uint32_t);

typedef void(* narwal_init)(int *result);
typedef int(* narwal_recv)(uint8_t *,uint32_t, struct narwal_sockaddr_in);
typedef int(* narwal_send)(uint8_t *,uint32_t);

// Enums
typedef enum{
	NARWAL_SOCKET_STATUS_IDLE = (uint8_t)0x00,
	NARWAL_SOCKET_STATUS_RECV = (uint8_t)0x01,
	NARWAL_SOCKET_STATUS_SEND = (uint8_t)0x02
}narwal_socket_status_t;


enum __narwal_socket_type{
	NARWAL_SOCK_STREAM = 1,		// Sequenced, reliable, connection-based byte streams.
//#define SOCK_STREAM NARWAL_SOCK_STREAM
	NARWAL_SOCK_DGRAM = 2,		// Connectionless, unreliable datagrams of fixed maximum length.
//#define SOCK_DGRAM NARWAL_SOCK_DGRAM
};

#define NARWAL_AF_INET		2
#define NARWAL_AF_INET6	10

// Address to accept any incoming messages.
#define	NARWAL_INADDR_ANY		((uint32_t) 0x00000000)
// Address to send to all hosts.
#define	NARWAL_INADDR_BROADCAST	((uint32_t) 0xffffffff)
// Address indicating an error return.
#define	NARWAL_INADDR_NONE		((uint32_t) 0xffffffff)


#define NARWAL_SOCKET_FLAG_NOWAIT	0x01

// Structs
struct narwal_os_driver{
	narwal_osThreadCreate    threadCreate;
	narwal_osThreadExit      threadExit;
	narwal_osJoin            threadJoin;

	narwal_osSemaphoreCreate semaphoreCreate;
	narwal_osSemaphoreTake   semaphoreTake;
	narwal_osSemaphoreGive   semaphoreGive;
	narwal_osSemaphoreCount  sempahoreGetCount;

	narwal_osMutexCreate     mutexCreate;
	narwal_osMutexLock       mutexLock;
	narwal_osMutexUnlock     mutexUnlock;

	narwal_osGetCurrentTicks getTicks;
	narwal_freeData          freeData;
	narwal_copyMem           copyMem;
	narwal_getRandom         getRandom;
	narwal_delay             delayMs;
};

struct narwal_if_driver{
	narwal_init init;
	narwal_send send;
};

struct narwal_pending_tasks{
	void *task;
	uint32_t when;
};

struct narwal_socket_handler_t{
	int32_t socketID;
	uint8_t sin_family;
	uint16_t sin_port;
	uint16_t din_port;
	narwal_recv callback;
	narwalSemaphoreHandler sendSemaphore __attribute__((aligned(4)));
	narwalSemaphoreHandler recvSemaphore __attribute__((aligned(4)));
	uint32_t nextAvailableBuffer;
	void *availableBuffers[NARWAL_RECV_POOL_SIZE];
	uint32_t availableLength[NARWAL_RECV_POOL_SIZE];

	struct narwal_addr bind;
	struct narwal_sockaddr_in sender;
};

// Bit manipulation
inline uint32_t narwal_htonl(uint32_t hostlong){ return HTOL(hostlong); }
inline uint16_t narwal_htons(uint16_t hostshort){ return HTOS(hostshort); }
inline uint32_t narwal_ntohl(uint32_t netlong){ return HTOL(netlong); }
inline uint16_t narwal_ntohs(uint16_t netshort){ return HTOS(netshort); }

// Narwal exclusive methods
void narwal_addArpEntry(struct narwal_addr who);
struct narwal_addr *narwal_getArpEntry(struct narwal_addr addr);
void narwal_zeroArpEntry(struct narwal_addr *entry);

int narwal_matchHwAddr(struct narwal_hwAddr a, struct narwal_hwAddr b);
void narwal_copyHwAddr(struct narwal_hwAddr *dest, struct narwal_hwAddr src);
struct narwal_hwAddr narwal_getHwAddr(void);

struct narwal_os_driver *narwal_getOsDriver(void);
struct narwal_if_driver *narwal_getIfDriver(void);
void narwal_signalReceivedPackage(uint8_t *buffer, uint32_t length);
uint8_t *narwal_getNextSendBuffer(void);
void narwal_signalSendPackage(uint8_t *buffer, uint32_t length);

int narwal_initos(struct narwal_os_driver driver);
int narwal_initif(struct narwal_if_driver driver, struct narwal_hwAddr addr);
void narwal_join(void);
int narwal_postTask(void *task, uint32_t when);
int narwal_cancelTask(void *task);
void narwal_recvForSocket(uint8_t *buffer, uint32_t length, uint8_t protocol, uint16_t destPort, struct narwal_sockaddr_in *sender);
void narwal_socketCallback(int __fd, narwal_recv __f);

void narwal_setHostname(const char *name);
char *narwal_getHostname(void);
uint8_t narwal_getHostnameLength(void);

int narwal_socket(int __domain, int __type, int __protocol);
//int narwal_socketpair (int __domain, int __type, int __protocol, int __fds[2]);
int narwal_bind(int __fd, struct narwal_sockaddr_in __addr, socklen_t __len);
//int narwal_getsockname (int __fd, __SOCKADDR_ARG __addr, socklen_t *__restrict __len);
//int narwal_connect (int __fd, __CONST_SOCKADDR_ARG __addr, socklen_t __len);
//int narwal_getpeername (int __fd, __SOCKADDR_ARG __addr, socklen_t *__restrict __len);
//ssize_t narwal_send (int __fd, const void *__buf, size_t __n, int __flags);
//ssize_t narwal_recv (int __fd, void *__buf, size_t __n, int __flags);
ssize_t narwal_sendto(int __fd, const void *__buf, size_t __n, int __flags, struct narwal_sockaddr_in __addr, socklen_t __addr_len);
ssize_t narwal_recvfrom(int __fd, void *__restrict __buf, size_t __n, int __flags, struct narwal_sockaddr_in *__addr, socklen_t *__restrict __addr_len);
//ssize_t narwal_sendmsg (int __fd, const struct msghdr *__message, int __flags);
//ssize_t narwal_recvmsg (int __fd, struct msghdr *__message, int __flags);
//int narwal_getsockopt (int __fd, int __level, int __optname, void *__restrict __optval, socklen_t *__restrict __optlen);
//int narwal_setsockopt (int __fd, int __level, int __optname, const void *__optval, socklen_t __optlen);
//int narwal_listen (int __fd, int __n);
//int narwal_accept (int __fd, __SOCKADDR_ARG __addr, socklen_t *__restrict __addr_len);
//int narwal_shutdown (int __fd, int __how);
int narwal_close(int __fd);

#endif // _INC_NARWAL_H_
