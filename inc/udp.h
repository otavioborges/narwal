/*
 * udp.h
 *
 *  Created on: Dec. 29, 2020
 *      Author: root
 */

#ifndef INC_UDP_H_
#define INC_UDP_H_

#define NARWAL_PACKET_UDP_LENGTH     8

#include "narwal.h"

typedef struct{
	uint16_t sourcePort;
	uint16_t destPort;
	uint16_t length;
	uint16_t checksum;
}udp_header_t;

void NARWAL_udp_parsePackage(uint8_t *buffer, size_t length, struct narwal_addr sender);
int NARWAL_udp_createPackage(struct narwal_addr who, uint16_t srcPort, uint16_t destPort, uint8_t *buffer, const uint8_t *payload, uint32_t payloadLength);

#endif /* INC_UDP_H_ */
