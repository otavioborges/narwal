/*
 * narwal_opt.h
 *
 *  Created on: Dec. 28, 2020
 *      Author: root
 */

#ifndef INC_NARWAL_OPT_H_
#define INC_NARWAL_OPT_H_

//#define NARWAL_OS_PTHREAD
#define NARWAL_OS_FREERTOS

#define NARWAL_THREAD_STACK_SIZE    512
#define NARWAL_PACKAGES_MAX_LENGTH	2048

#define NARWAL_RECV_POOL_SIZE       5
#define NARWAL_SEND_POOL_SIZE		5

// If you want to use dynamic memory. Don't forget to define the dynamic handlers on OS struct
//#define NARWAL_MEM_STATIC_ENABLED

#define NARWAL_IPv4_ENABLED
//#define NARWAL_IPv6_ENABLED

#define NARWAL_HW_ADDR_LENGTH       6

#define NARWAL_SOCKET_MAX_RETRIES   10

#define NARWAL_HOSTNAME_ENABLED

#define NARWAL_TCP_SOCKETS_ENABLED
#define NARWAL_DHCP_CLIENT_ENABLED
#define NARWAL_NTP_CLIENT_ENABLED

#ifndef NARWAL_UDP_SOCKETS_COUNT
#define NARWAL_UDP_SOCKETS_COUNT    3
#endif

#ifdef NARWAL_TCP_SOCKETS_ENABLED
#define NARWAL_TCP_SOCKETS_COUNT    3
#endif

#ifndef NARWAL_MEM_DYNAMIC_ENABLED
#define NARWAL_MEM_STATIC_ENABLED
#endif

#define NARWAL_ARP_BUFFER_LENGTH    256U

// DHCP client definitions
#ifdef NARWAL_DHCP_CLIENT_ENABLED
#define NARWAL_DHCP_BUFFER_LENGTH	1024U
#define NARWAL_DHCP_DEFAULT_RENEWAL 43200U // 12 hours before renewal
#endif

#ifdef NARWAL_HOSTNAME_ENABLED
#define NARWAL_HOSTNAME_LENGTH      32
#endif

#ifndef NARWAL_DNS_SERVER_COUNT
#define NARWAL_DNS_SERVER_COUNT     2
#endif

#if defined(NARWAL_OS_PTHREAD) && defined(NARWAL_OS_FREERTOS)
#error "Select only one Operation System"
#endif

#ifdef NARWAL_OS_PTHREAD
#include <pthread.h>
#include <semaphore.h>
typedef pthread_t                   narwalThreadHandler;
typedef sem_t                       narwalSemaphoreHandler;
typedef pthread_mutex_t             narwalMutexHandler;
#endif

#ifdef NARWAL_OS_FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
typedef TaskHandle_t                narwalThreadHandler;
typedef SemaphoreHandle_t           narwalSemaphoreHandler;
typedef SemaphoreHandle_t           narwalMutexHandler;

//typedef uint32_t                    ssize_t;
#endif

#endif /* INC_NARWAL_OPT_H_ */
