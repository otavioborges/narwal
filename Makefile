.PHONY: clean
.PHONY: dirs

BINDIR=bin
SRCDIR=src
TESTDIR=test
%TARGET=$(BINDIR)/libnarwal.a
TARGET=$(BINDIR)/narwal
CFLAGS=-Iinc -g3
LDFLAGS=-lpthread
SRCFILES=$(wildcard $(SRCDIR)/*.c)
OBJFILES=$(patsubst $(SRCDIR)/%.c,$(BINDIR)/%.o,$(SRCFILES))

all: dirs $(TARGET)

dirs:
	mkdir -p $(BINDIR)

$(TARGET): $(OBJFILES)
	gcc $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(BINDIR)/%.o: $(SRCDIR)/%.c
	gcc $(CFLAGS) -c -o $@ $^
	
clean:
	rm -rf $(TARGET) $(OBJFILES)

