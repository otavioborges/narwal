#include "narwal.h"

#ifdef NARWAL_IPv4_ENABLED
#include "ipv4.h"
#else

#endif

#include "eth.h"
#include "arp.h"
#include "udp.h"
#include "tcp.h"

#define NARWAL_TOTAL_SOCKET_COUNT    (NARWAL_SYSTEM_SOCKET_COUNT + NARWAL_UDP_SOCKETS_COUNT + NARWAL_TCP_SOCKETS_COUNT)

#include <stddef.h>

static struct narwal_os_driver g_narwalOsDriver = {NULL};
static struct narwal_if_driver g_narwalIfDriver = {NULL, NULL};

static uint8_t g_keepRunning = 0;
static struct narwal_hwAddr g_ourMAC;
static uint32_t g_arpTableCount;
static struct narwal_addr g_arpTable[NARWAL_ARP_TABLE_LENGTH];

static narwalThreadHandler g_tcpipHandler;
static narwalSemaphoreHandler g_availRecv __attribute__((aligned(4)));
static size_t g_recvLength[NARWAL_RECV_POOL_SIZE];

static narwalMutexHandler g_pendingMutex __attribute__((aligned(4)));
static uint32_t g_pendingTasksCount;
static struct narwal_pending_tasks g_pendingTasks[NARWAL_PENDING_TASKS_LENGTH];

static narwalSemaphoreHandler g_availSend __attribute__((aligned(4)));
static narwalMutexHandler g_sendBufferMutex __attribute__((aligned(4)));
static size_t g_sendLength[NARWAL_SEND_POOL_SIZE];
#ifndef NARWAL_MEM_DYNAMIC_ENABLED
static uint8_t g_sendBuffers[NARWAL_SEND_POOL_SIZE][NARWAL_PACKAGES_MAX_LENGTH]  __attribute__((aligned(4)));
#else
static uint8_t *g_sendBuffers[NARWAL_SEND_POOL_SIZE];
#endif

// Global variables for sockets
static narwalMutexHandler g_socketsMutex __attribute__((aligned(4)));
static uint32_t g_socketCount;
static struct narwal_socket_handler_t g_socketHandlers[NARWAL_TOTAL_SOCKET_COUNT] __attribute__((aligned(4)));

static const struct narwal_addr g_broadcastAddr = {
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
		NARWAL_INADDR_BROADCAST
};


#ifdef NARWAL_HOSTNAME_ENABLED
// Default HOST NAME
static uint8_t g_hostnameLength = 6;
static char g_hostname[NARWAL_HOSTNAME_LENGTH] = "narwal";
#else
static uint8_t g_hostnameLength = 0;
static char *g_hostname = NULL;
#endif

static void narwal_tcpipThread(void *argument);

static void narwal_releaseBuffer(uint8_t *buffer, uint8_t protocol){
	buffer -= NARWAL_PACKET_ETH_LENGTH;
#ifdef NARWAL_IPv4_ENABLED
	buffer -= NARWAL_PACKET_IPv4_LENGTH;
#endif

	switch(protocol){
	case NARWAL_IPv4_PROTOCOL_UDP:
		buffer -= NARWAL_PACKET_UDP_LENGTH;
		break;
	case NARWAL_IPv4_PROTOCOL_TCP:
		buffer -= NARWAL_PACKET_TCP_LENGTH;
		break;
	}

	g_narwalOsDriver.freeData(buffer);
}

void narwal_addArpEntry(struct narwal_addr who){
	for(uint32_t idx = 0; idx < g_arpTableCount; idx++){
#ifdef NARWAL_IPv4_ENABLED
		if(NARWAL_ipv4_matchAddr(g_arpTable[idx].s_addr, who.s_addr)){
			g_arpTable[idx].s_addr = who.s_addr;

			return;
		}
#else

#endif
	}

	// IP is not yet ARP table
	narwal_copyHwAddr(&(g_arpTable[g_arpTableCount].mac), who.mac);
#ifdef NARWAL_IPv4_ENABLED
	g_arpTable[g_arpTableCount].s_addr = who.s_addr;
#else

#endif

	g_arpTableCount++;
}

struct narwal_addr *narwal_getArpEntry(struct narwal_addr addr){
	for(uint32_t idx = 0; idx < g_arpTableCount; idx++){
#ifdef NARWAL_IPv4_ENABLED
		if(g_arpTable[idx].s_addr == addr.s_addr)
			return (g_arpTable + idx);
#else
		// We have to implement this
#endif
	}

	return NULL;
}

void narwal_zeroArpEntry(struct narwal_addr *entry){
	for(uint32_t hIdx = 0; hIdx < NARWAL_HW_ADDR_LENGTH; hIdx++){
		entry->mac.w[hIdx] = 0;
	}

	entry->s_addr = 0;
}

int narwal_matchHwAddr(struct narwal_hwAddr a, struct narwal_hwAddr b){
	for(uint8_t idx = 0; idx < NARWAL_HW_ADDR_LENGTH; idx++){
		if(a.w[idx] != b.w[idx])
			return 0;
	}

	// all match
	return 1;
}

void narwal_copyHwAddr(struct narwal_hwAddr *dest, struct narwal_hwAddr src){
	for(uint8_t idx = 0; idx < NARWAL_HW_ADDR_LENGTH; idx++)
		dest->w[idx] = src.w[idx];
}

// TODO: use struct narwal_hwAddr for all MACs
struct narwal_hwAddr narwal_getHwAddr(void){
	return g_ourMAC;
}

struct narwal_os_driver *narwal_getOsDriver(void){
	return &g_narwalOsDriver;
}

struct narwal_if_driver *narwal_getIfDriver(void){
	return &g_narwalIfDriver;
}

void narwal_signalReceivedPackage(uint8_t *buffer, uint32_t length){
//	int32_t nextBuffer = -1;
//	for(uint32_t rIdx = 0; rIdx < NARWAL_RECV_POOL_SIZE; rIdx++){
//		if(!g_recvData[rIdx]){
//			nextBuffer = rIdx;
//			break;
//		}
//	}
//
//	if(nextBuffer >= 0){
//		g_recvData[nextBuffer] = buffer;
//		g_recvLength[nextBuffer] = length;
//
//		// Try parsing the package
//		NARWAL_eth_parsePackage(g_recvData[nextBuffer], length);
//	}

	if(NARWAL_eth_parsePackage(buffer, length) == 0){
		// we can free this baby
//		g_narwalOsDriver.freeData(buffer);
	}
}

uint8_t *narwal_getNextSendBuffer(void){
	g_narwalOsDriver.mutexLock(&g_sendBufferMutex);
	for(uint32_t sIdx = 0; sIdx < NARWAL_SEND_POOL_SIZE; sIdx++){
		if(g_sendLength[sIdx] == 0)
			return g_sendBuffers[sIdx];	// Don't unlock for now, someone will use this buffer
	}

	// no buffers available
	g_narwalOsDriver.mutexUnlock(&g_sendBufferMutex);
	return NULL;
}

void narwal_signalSendPackage(uint8_t *buffer, uint32_t length){
	for(uint32_t sIdx = 0; sIdx < NARWAL_SEND_POOL_SIZE; sIdx++){
			if(g_sendBuffers[sIdx] == buffer){
				g_sendLength[sIdx] = length;
			}
	}
	g_narwalOsDriver.semaphoreGive(&g_availSend);
	g_narwalOsDriver.mutexUnlock(&g_sendBufferMutex);
}

int narwal_initos(struct narwal_os_driver driver){
	g_narwalOsDriver = driver;

	return NARWAL_ERROR_OK;
}

int narwal_initif(struct narwal_if_driver driver, struct narwal_hwAddr addr){
	int result, idx;

	g_narwalIfDriver = driver;
	narwal_copyHwAddr(&g_ourMAC, addr);

	g_narwalIfDriver.init(&result);
	if(result < 0){
		return NARWAL_ERROR_INIT_FAILED;
	}

	if((!g_narwalOsDriver.threadCreate) | (!g_narwalOsDriver.semaphoreCreate) | (!g_narwalOsDriver.semaphoreGive) | (!g_narwalOsDriver.semaphoreTake))
		return NARWAL_ERROR_BAD_DRIVER;

	// Zero the arp table
	g_arpTableCount = 0;
	for(idx = 0; idx < NARWAL_ARP_TABLE_LENGTH; idx++){

		narwal_zeroArpEntry((g_arpTable + idx));
	}

	// Zero the pending tasks
	g_pendingTasksCount = 0;
	for(idx = 0; idx < NARWAL_PENDING_TASKS_LENGTH; idx++){
		g_pendingTasks[idx].task = NULL;
		g_pendingTasks[idx].when = 0;
	}

	// Zero received packages
	for(idx = 0; idx < NARWAL_RECV_POOL_SIZE; idx++){
		g_recvLength[idx] = 0;
	}

	// Zero the socket handlers
	g_socketCount = 0;
	for(idx = 0; idx < NARWAL_TOTAL_SOCKET_COUNT; idx++){
		g_socketHandlers[idx].socketID = -1;
	}

	g_narwalOsDriver.semaphoreCreate(&g_availRecv, NARWAL_RECV_POOL_SIZE, 0, &result);
	NARWAL_ASSERT_RESULT(result);

	g_narwalOsDriver.semaphoreCreate(&g_availSend, NARWAL_SEND_POOL_SIZE, 0, &result);
	NARWAL_ASSERT_RESULT(result);

	g_narwalOsDriver.mutexCreate(&g_sendBufferMutex, &result);
	NARWAL_ASSERT_RESULT(result);

	g_narwalOsDriver.mutexCreate(&g_pendingMutex, &result);
	NARWAL_ASSERT_RESULT(result);

	g_narwalOsDriver.mutexCreate(&g_socketsMutex, &result);
	NARWAL_ASSERT_RESULT(result);

	g_keepRunning = 1;
	g_narwalOsDriver.threadCreate(&g_tcpipHandler, narwal_tcpipThread, (void *)&g_keepRunning, NARWAL_THREAD_STACK_SIZE, &result);
	NARWAL_ASSERT_RESULT(result);

	return NARWAL_ERROR_OK;
}

void narwal_join(void){
	g_keepRunning = 0;
	g_narwalOsDriver.threadJoin(&g_tcpipHandler);
}

void narwal_tcpipThread(void *argument){
	uint32_t packageCount;
	void (*nextTask)(void) = NULL;
	uint32_t nextSend = 0;
	uint8_t *keep = (uint8_t *)argument;

	while(*keep){
		// Check if any pending tasks and execute then
		for(uint32_t tIdx = 0; tIdx < NARWAL_PENDING_TASKS_LENGTH; tIdx++){
			g_narwalOsDriver.mutexLock(&g_pendingMutex);
			if(g_pendingTasks[tIdx].task){
				if(g_pendingTasks[tIdx].when <= g_narwalOsDriver.getTicks()){
					nextTask = g_pendingTasks[tIdx].task;
					g_pendingTasks[tIdx].task = NULL;
					g_pendingTasks[tIdx].when = 0;
				}
			}
			g_narwalOsDriver.mutexUnlock(&g_pendingMutex);

			if(nextTask){
				nextTask();
				nextTask = NULL;
			}
		}

		// Check if any packages are left to be send
		packageCount = g_narwalOsDriver.sempahoreGetCount(&g_availSend);
		if(packageCount){
			// Send queued packages
			for(int32_t sIdx = (packageCount - 1); sIdx >= 0; sIdx--){
				g_narwalOsDriver.mutexLock(&g_sendBufferMutex);
				if(g_sendLength[sIdx] > 0){
					if(g_sendLength[sIdx] < 60){
						// Pad with zeros
						for(uint32_t pad = g_sendLength[sIdx]; pad < 60; pad++)
							g_sendBuffers[sIdx][pad] = 0;

						g_sendLength[sIdx] = 60;
					}
					g_narwalIfDriver.send(g_sendBuffers[sIdx], g_sendLength[sIdx]);
					g_sendLength[sIdx] = 0;
				}
				g_narwalOsDriver.mutexUnlock(&g_sendBufferMutex);
				g_narwalOsDriver.semaphoreTake(&g_availSend, 0, NULL);
			}
		}
	}

	g_narwalOsDriver.threadExit(NULL);
}

int narwal_postTask(void *task, uint32_t when){
	int32_t firstFree = -1;
	g_narwalOsDriver.mutexLock(&g_pendingMutex);

	when += g_narwalOsDriver.getTicks();
	if(when < g_narwalOsDriver.getTicks())
		when = 0xFFFFFFFFU;				// Avoid timeout being overflow

	for(uint32_t tIdx = 0; tIdx < NARWAL_PENDING_TASKS_LENGTH; tIdx++){
		if(g_pendingTasks[tIdx].task == task){
			g_pendingTasks[tIdx].task = task;
			g_pendingTasks[tIdx].when = when;

			g_narwalOsDriver.mutexUnlock(&g_pendingMutex);
			return NARWAL_ERROR_OK;
		}

		if(firstFree < 0){
			if(!g_pendingTasks[tIdx].task)
				firstFree = tIdx;
		}
	}

	if(firstFree >= 0){
		g_pendingTasks[firstFree].task = task;
		g_pendingTasks[firstFree].when = when;

		g_narwalOsDriver.mutexUnlock(&g_pendingMutex);
		return NARWAL_ERROR_OK;
	}

	g_narwalOsDriver.mutexUnlock(&g_pendingMutex);
	return NARWAL_ERROR_FULL_BUFFER;
}

int narwal_cancelTask(void *task){
	g_narwalOsDriver.mutexLock(&g_pendingMutex);

	for(uint32_t tIdx = 0; tIdx < NARWAL_PENDING_TASKS_LENGTH; tIdx++){
		if(g_pendingTasks[tIdx].task == task){
			g_pendingTasks[tIdx].task = NULL;
			g_pendingTasks[tIdx].when = 0;

			g_narwalOsDriver.mutexUnlock(&g_pendingMutex);
			return NARWAL_ERROR_OK;
		}
	}

	g_narwalOsDriver.mutexUnlock(&g_pendingMutex);
	return NARWAL_ERROR_NOT_FOUND;
}

void narwal_recvForSocket(uint8_t *buffer, uint32_t length, uint8_t protocol, uint16_t destPort, struct narwal_sockaddr_in *sender){
	int result = 0;
	uint32_t *nextAvailBuffer;
	struct narwal_socket_handler_t *handler = NULL;

	g_narwalOsDriver.mutexLock(&g_socketsMutex);
	for(uint32_t idx = 0; idx < g_socketCount; idx++){
		if(g_socketHandlers[idx].socketID >= 0){
			if(g_socketHandlers[idx].sin_family == protocol){
				if(g_socketHandlers[idx].sin_port == destPort){
					handler = (g_socketHandlers + idx);
					break;
				}
			}
		}
	}
	g_narwalOsDriver.mutexUnlock(&g_socketsMutex);

	if(handler){
		if(handler->callback){
			handler->callback(buffer, length, *sender);
			// Data has been processed, free
//			narwal_releaseBuffer(buffer, protocol);
		}else{
			nextAvailBuffer = &(handler->nextAvailableBuffer);

			handler->availableBuffers[*nextAvailBuffer] = buffer;
			handler->availableLength[*nextAvailBuffer]  = length;

			g_narwalOsDriver.copyMem(&(handler->sender), sender, sizeof(struct narwal_sockaddr_in));
			(*nextAvailBuffer)++;
			if(*nextAvailBuffer > NARWAL_RECV_POOL_SIZE)
				*nextAvailBuffer = 0;

			g_narwalOsDriver.semaphoreGive(&(handler->recvSemaphore));
		}
	}
}

void narwal_socketCallback(int __fd, narwal_recv __f){
	g_narwalOsDriver.mutexLock(&g_socketsMutex);
	for(uint32_t idx = 0; idx < g_socketCount; idx++){
		if(g_socketHandlers[idx].socketID == __fd){
			g_socketHandlers[idx].callback = __f;
		}
	}
	g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
}

void narwal_setHostname(const char *name){
#ifdef NARWAL_HOSTNAME_ENABLED
	g_hostnameLength = 0;

	while(name[g_hostnameLength] != '\0'){
		g_hostname[g_hostnameLength] = name[g_hostnameLength];
		g_hostnameLength++;

		if(g_hostnameLength == 0xFFU)
			break;
	}

	g_hostname[g_hostnameLength] = '\0';
#endif
}

char *narwal_getHostname(void){
#ifdef NARWAL_HOSTNAME_ENABLED
	return g_hostname;
#else
	return 0;
#endif
}

uint8_t narwal_getHostnameLength(void){
	return g_hostnameLength;
}

int narwal_socket(int __domain, int __type, int __protocol){
	struct narwal_socket_handler_t *handler;
	int result;

#ifdef NARWAL_IPv4_ENABLED
	if(__domain != NARWAL_AF_INET)
		return -1;
#else
	if(__domain != NARWAL_AF_INET6)
		return -1;
#endif

	// Check if there's space available for a socket
	// TODO: check if socket is comming from system call. Always keep a space for those
	if(g_socketCount >= NARWAL_TOTAL_SOCKET_COUNT)
		return -2;

	g_narwalOsDriver.mutexLock(&g_socketsMutex);
	g_socketCount++;
	for(uint32_t idx = 0; idx < g_socketCount; idx++){
		if(g_socketHandlers[idx].socketID < 0)
			handler = (g_socketHandlers + idx);
	}
	handler->socketID = (g_socketCount - 1);

	if(__type == NARWAL_SOCK_STREAM){
		handler->sin_family = NARWAL_IPv4_PROTOCOL_TCP;
	}else if(__type == NARWAL_SOCK_DGRAM){
		handler->sin_family = NARWAL_IPv4_PROTOCOL_UDP;
	}else{
		handler->socketID = -1;

		g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
		return -3;
	}

	// We need a bind to define the port
	handler->sin_port = 0;
	handler->din_port = 0;

	handler->callback = NULL;
	handler->nextAvailableBuffer = 0;

	for(uint32_t idx = 0; idx < NARWAL_RECV_POOL_SIZE; idx++){
		handler->availableBuffers[idx] = NULL;
		handler->availableLength[idx] = 0;
	}

	g_narwalOsDriver.semaphoreCreate(&(handler->recvSemaphore), NARWAL_RECV_POOL_SIZE, 0, &result);
	if(result != NARWAL_ERROR_OK){
		// couldn't create a mutex release socket
		handler->socketID = -1;

		g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
		return -4;
	}

	g_narwalOsDriver.semaphoreCreate(&(handler->sendSemaphore), NARWAL_SEND_POOL_SIZE, 0, &result);
	if(result != NARWAL_ERROR_OK){
		// couldn't create a mutex release socket
		handler->socketID = -1;

		g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
		return -5;
	}

	// no sender, no bind
	handler->bind.s_addr = 0;
	handler->sender.sin_addr.s_addr = 0;

	//all good return socket ID
	g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
	return handler->socketID;
}

int narwal_bind(int __fd, struct narwal_sockaddr_in __addr, socklen_t __len){
	g_narwalOsDriver.mutexLock(&g_socketsMutex);
	for(uint32_t idx = 0; idx < g_socketCount; idx++){
		if(g_socketHandlers[idx].socketID == __fd){
			g_narwalOsDriver.copyMem(&(g_socketHandlers[idx].bind), &(__addr.sin_addr), sizeof(struct narwal_addr));
			g_socketHandlers[idx].sin_port = __addr.sin_port;

			g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
			return __fd; // We are ok!
		}
	}

	g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
	return -1; // Oh oh!
}

int narwal_close(int __fd){
	g_narwalOsDriver.mutexLock(&g_socketsMutex);
	for(uint32_t idx = 0; idx < g_socketCount; idx++){
		if(g_socketHandlers[idx].socketID == __fd){
			g_socketHandlers[idx].socketID = -1;
			g_socketCount--;

			g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
			return __fd; // We are ok!
		}
	}

	g_narwalOsDriver.mutexUnlock(&g_socketsMutex);
	return -1; // Oh oh!
}

ssize_t narwal_sendto (int __fd, const void *__buf, size_t __n, int __flags, struct narwal_sockaddr_in __addr, socklen_t __addr_len){
	struct narwal_socket_handler_t *handler = NULL;
	struct narwal_addr *zeAddr = NULL;
	uint32_t retries, dataSend;

	g_narwalOsDriver.mutexLock(&g_socketsMutex);
	for(uint32_t idx = 0; idx < g_socketCount; idx++){
		if(g_socketHandlers[idx].socketID == __fd){
			handler = (g_socketHandlers + idx);
			break;
		}
	}
	g_narwalOsDriver.mutexUnlock(&g_socketsMutex);

	if(!handler)
		return -1;	// Thats not a socket

	if(!handler->sin_port)
		return -3;	// Socket is not bound

	if(!__addr.sin_addr.s_addr)
		return -4; // We cannot send to 0

	// Check if we have this IP on the ARP table, if not broadcast
	if(__addr.sin_addr.s_addr != NARWAL_INADDR_BROADCAST){
		zeAddr = narwal_getArpEntry(__addr.sin_addr);
		if(!zeAddr){ // no entry, add to arp table
			g_narwalOsDriver.mutexLock(&g_sendBufferMutex);
			for(uint32_t sIdx = 0; sIdx < NARWAL_SEND_POOL_SIZE; sIdx++){
				if(g_sendLength[sIdx] == 0){
					g_sendLength[sIdx] = NARWAL_arp_createPackage(__addr.sin_addr, g_sendBuffers[sIdx]);

					if(g_sendLength[sIdx])
						g_narwalOsDriver.semaphoreGive(&g_availSend);

					break;
				}
			}

			g_narwalOsDriver.mutexUnlock(&g_sendBufferMutex);
			retries = 0;
			while(retries++ < NARWAL_SOCKET_MAX_RETRIES){
				g_narwalOsDriver.delayMs(NARWAL_TIMEOUT);

				zeAddr = narwal_getArpEntry(__addr.sin_addr);
				if(zeAddr)
					break;
			}

			if(!zeAddr)
				return -5;	// Could not find IP
		}
	}else{
		zeAddr = (struct narwal_addr *)&g_broadcastAddr;
	}

	// All set, send the package
	g_narwalOsDriver.mutexLock(&g_sendBufferMutex);
	for(uint32_t sIdx = 0; sIdx < NARWAL_SEND_POOL_SIZE; sIdx++){
		if(g_sendLength[sIdx] == 0){
			// TODO: replace this with a nice auto-package assembler
//			if(__addr.sin_family == NARWAL_IPv4_PROTOCOL_UDP)
			if(handler->sin_family == NARWAL_IPv4_PROTOCOL_UDP)
				g_sendLength[sIdx] = NARWAL_udp_createPackage(*zeAddr, handler->sin_port, __addr.sin_port, g_sendBuffers[sIdx], __buf, (uint32_t)__n);

			if(g_sendLength[sIdx])
				g_narwalOsDriver.semaphoreGive(&g_availSend);

			dataSend = g_sendLength[sIdx];
			break;
		}
	}

	g_narwalOsDriver.mutexUnlock(&g_sendBufferMutex);

	return dataSend;
}

ssize_t narwal_recvfrom(int __fd, void *__restrict __buf, size_t __n, int __flags, struct narwal_sockaddr_in *__addr, socklen_t *__restrict __addr_len){
	struct narwal_socket_handler_t *handler = NULL;
	struct narwal_addr *zeAddr = NULL;
	uint32_t retries, dataRecv;
	int result;

	g_narwalOsDriver.mutexLock(&g_socketsMutex);
	for(uint32_t idx = 0; idx < g_socketCount; idx++){
		if(g_socketHandlers[idx].socketID == __fd){
			handler = (g_socketHandlers + idx);
			break;
		}
	}
	g_narwalOsDriver.mutexUnlock(&g_socketsMutex);

	if(!handler)
		return -1;	// Thats not a socket

	if(!handler->sin_port)
		return -3;	// Socket is not bound

	// Wait for a available message
	g_narwalOsDriver.semaphoreTake(&(handler->recvSemaphore), 30000, &result);
//	g_narwalOsDriver.semaphoreTake(&(handler->recvSemaphore), 0, &result);
	if(result < 0)
		return -4;

	for(uint32_t idx = 0; idx < NARWAL_RECV_POOL_SIZE; idx++){
		if(handler->availableLength[idx]){
			if(handler->availableLength[idx] > __n){
				g_narwalOsDriver.copyMem(__buf, handler->availableBuffers[idx], __n);
				dataRecv = __n;
				handler->availableBuffers[idx] += __n;
				handler->availableLength[idx] -= __n;

				// there's still data, release semaphore
				g_narwalOsDriver.semaphoreGive(&(handler->recvSemaphore));
			}else{
				g_narwalOsDriver.copyMem(__buf, handler->availableBuffers[idx], handler->availableLength[idx]);
				dataRecv = handler->availableLength[idx];

//				narwal_releaseBuffer(handler->availableBuffers[idx], handler->sin_family);
				handler->availableBuffers[idx] = NULL;
				handler->availableLength[idx] = 0;

			}

			g_narwalOsDriver.copyMem(__addr, &(handler->sender), sizeof(struct narwal_sockaddr_in));
			*__addr_len = sizeof(struct narwal_sockaddr_in);
		}
	}

	return dataRecv;
}
