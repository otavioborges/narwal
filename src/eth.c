/*
 * eth.c
 *
 *  Created on: Dec. 28, 2020
 *      Author: root
 */

#include "eth.h"

#include "ipv4.h"
#include "arp.h"

const struct narwal_hwAddr NARWAL_ETH_BROADCAST = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
const struct narwal_hwAddr NARWAL_ETH_NONE = {0, 0, 0, 0, 0, 0};

int NARWAL_eth_parsePackage(uint8_t *buffer, size_t length){
	eth_header_t *header = (eth_header_t *)buffer;
	uint8_t *payload = (buffer + NARWAL_PACKET_ETH_LENGTH);
	struct narwal_addr sender;

	narwal_copyHwAddr(&(sender.mac), *((struct narwal_hwAddr *)header->sourceMAC));
	sender.s_addr = 0;	// Ip is not yet on this layer

	// Check if package is for us or broadcast
	if(narwal_matchHwAddr(*(struct narwal_hwAddr *)(header->destMAC), narwal_getHwAddr()) == 0){
		// nothing to do if package is not boradcast at this point
		if(narwal_matchHwAddr(*(struct narwal_hwAddr *)(header->destMAC), NARWAL_ETH_BROADCAST) == 0)
			return 0;
	}

	switch(HTOS(header->etherType)){
	case NARWAL_ETHERTYPE_IPv4:
		NARWAL_ipv4_parsePackage(payload, (length - NARWAL_PACKET_ETH_LENGTH), sender);
		return 1; // if the package is a ipv4, parsing will be done on a socket, do not free
	case NARWAL_ETHERTYPE_ARP:
		NARWAL_arp_parsePackage(payload, (length - NARWAL_PACKET_ETH_LENGTH), sender);
		return 0;
	}

	// protocol not supported
	return 0;
}

int NARWAL_eth_createHeader(uint8_t *buffer, const uint8_t *destAddr, const uint8_t *srcAddr, eth_ethertype_t etherType){
	eth_header_t *header = (eth_header_t *)buffer;

	narwal_copyHwAddr((struct narwal_hwAddr *)header->destMAC, *((struct narwal_hwAddr *)destAddr));
	narwal_copyHwAddr((struct narwal_hwAddr *)header->sourceMAC, *((struct narwal_hwAddr *)srcAddr));
	header->etherType = (uint16_t)HTOS(etherType);

	return NARWAL_PACKET_ETH_LENGTH;
}
