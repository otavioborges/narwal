#include "narwal.h"

#include "eth.h"
#include "ipv4.h"
#include "udp.h"

void NARWAL_udp_parsePackage(uint8_t *buffer, size_t length, struct narwal_addr sender){
	udp_header_t *header = (udp_header_t *)buffer;
	uint8_t *payload = (buffer + NARWAL_PACKET_UDP_LENGTH);
	struct narwal_sockaddr_in addr;

	narwal_getOsDriver()->copyMem(addr.sin_addr.mac.w, sender.mac.w, NARWAL_HW_ADDR_LENGTH);
	addr.sin_addr.s_addr = sender.s_addr;
	addr.sin_port = header->sourcePort;

#ifdef NARWAL_IPv4_ENABLED
	addr.sin_family = NARWAL_AF_INET;
#else
	addr.sin_family = NARWAL_AF_INET6;
#endif

	// if there a socket waiting for this?
	narwal_recvForSocket(payload, (length - NARWAL_PACKET_UDP_LENGTH), NARWAL_IPv4_PROTOCOL_UDP, header->destPort, &addr);
}

int NARWAL_udp_createPackage(struct narwal_addr who, uint16_t srcPort, uint16_t destPort, uint8_t *buffer, const uint8_t *payload, uint32_t payloadLength){
	uint32_t currentLength = 0;
	udp_header_t *header;

	currentLength = NARWAL_eth_createHeader(buffer, who.mac.w, narwal_getHwAddr().w, NARWAL_ETHERTYPE_IPv4);

	currentLength += NARWAL_ipv4_createHeader((buffer + currentLength), who, NULL, (payloadLength + NARWAL_PACKET_UDP_LENGTH), 0x9348, NARWAL_IPv4_PROTOCOL_UDP);
	header = (udp_header_t *)(buffer + currentLength);

	currentLength += NARWAL_PACKET_UDP_LENGTH;
	header->sourcePort = srcPort;
	header->destPort = destPort;
	header->length = HTOS((payloadLength + NARWAL_PACKET_UDP_LENGTH));
	header->checksum = 0; // no checksum for now

	narwal_getOsDriver()->copyMem((buffer + currentLength), payload, payloadLength);
	currentLength += payloadLength;

	return currentLength;
}
