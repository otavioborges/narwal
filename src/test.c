#define ASSERT(x)            if(x) return x

#define BUFFER_LENGTH        1024U

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include <errno.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/if_ether.h>
#include <netpacket/packet.h>

#include "narwal.h"
#include "dhcp.h"
#include "ipv4.h"

static int PrintError(char *msg, int errorCode);
static int createRawSocket(const char *interface, int *socketFd);
static int getMACAddr(int socketFd, const char *interface, uint8_t *addr);
//static void GetIPaddr(void);

#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h>

int socketHandler;
int interfaceIndex;

void osThreadCreate(narwalThreadHandler *handler, void *routine, void *arg, uint32_t stackSize, int *result){
	if(pthread_create(handler, NULL, routine, arg) == 0)
		*result = NARWAL_ERROR_OK;
	else
		*result = NARWAL_ERROR_BAD_DRIVER;
}

void osThreadExit(narwalThreadHandler *handler){
	if(handler)
		pthread_cancel(*handler);
	else
		pthread_exit(NULL);
}

void osThreadJoin(narwalThreadHandler *handler){
	pthread_join(*handler, NULL);
}

void osSemaphoreCreate(narwalSemaphoreHandler *sem, int max, int initial, int *result){
	if(sem_init(sem, 0, initial) == 0)
		*result = NARWAL_ERROR_OK;
	else
		*result = NARWAL_ERROR_BAD_DRIVER;
}

void osSemaphoreTake(narwalSemaphoreHandler *sem, uint32_t delay, int *result){
	int rtnResult;
	struct timespec ts;

	if(delay){
		clock_gettime(CLOCK_REALTIME, &ts);
//		if(delay > 1000U)
			ts.tv_sec += delay;
//		ts.tv_nsec += (delay * 1000000U);

		rtnResult = sem_timedwait(sem, &ts);
	}else{
		rtnResult = sem_wait(sem);
	}

	if(result != NULL)
		*result = rtnResult;
}

void osSemaphoreGive(narwalSemaphoreHandler *sem){
	sem_post(sem);
}

int osSempahoreGetCount(narwalSemaphoreHandler *sem){
	int result;
	sem_getvalue(sem, &result);

	return result;
}

void osMutexCreate(narwalMutexHandler *mutex, int *result){
	if(pthread_mutex_init(mutex, NULL) == 0)
		*result = NARWAL_ERROR_OK;
	else
		*result = NARWAL_ERROR_BAD_DRIVER;
}

void osMutexLock(narwalMutexHandler *mutex){
	pthread_mutex_lock(mutex);
}

void osMutexUnlock(narwalMutexHandler *mutex){
	pthread_mutex_unlock(mutex);
}

uint32_t osGetTicks(void){
	struct timeval result;
	gettimeofday(&result, NULL);

	return (((uint32_t)time(NULL) * 1000) + (result.tv_usec / 1000));
}

void osFreeData(void *pointer){
	free(pointer);
}

void osCopyMem(void *dest , const void *src, uint32_t length){
	memcpy(dest, src, length);
}

uint32_t osGetRandom(void){
	return (uint32_t)rand();
}

void osDelay(uint32_t ms){
	usleep((ms * 1000));
}

int ifSend(uint8_t *buffer, uint32_t length){
	struct sockaddr_ll sendAddr;
	socklen_t sendLength = sizeof(sendAddr);
	char sendIP[32];

	sendAddr.sll_ifindex = 3;//interfaceIndex;
//	sendAddr.sll_ifindex = 4;
	sendAddr.sll_halen = ETH_ALEN;
	narwal_copyHwAddr((struct narwal_hwAddr *)sendAddr.sll_addr, *((struct narwal_hwAddr *)buffer));
	int result = sendto(socketHandler, buffer, length, 0, (struct sockaddr *)&sendAddr, sizeof(struct sockaddr_ll));
	if(result < 0){
		return NARWAL_ERROR_BAD_DRIVER;
	}else{
		printf("Package was sent, length: %d\n", length);
		return NARWAL_ERROR_OK;
	}
}

struct narwal_os_driver osDriver;
struct narwal_if_driver ifDriver;

void *recvThread(void *argument){
	uint32_t length;
	uint8_t *recvBuffer = NULL;
	uint8_t *running = (uint8_t *)argument;
	struct sockaddr recvAddr;
	socklen_t recvLength = sizeof(recvAddr);

	while(*running){
		recvBuffer = (uint8_t *)malloc(2048);
		length = recvfrom(socketHandler, recvBuffer, 2048, 0, &recvAddr, &recvLength);
		if(length < 0){
			free(recvBuffer);
		}else{
			narwal_signalReceivedPackage(recvBuffer, length);
		}
	}
}

int main(int argc, char **argv){
	pthread_t recvHandler;
	uint8_t keepRunning;

//	const char opt[] = "enp3s0";
	const char opt[] = "enp2s0";
	int result = 0;

	result = createRawSocket(opt, &socketHandler);
	ASSERT(result);

	struct narwal_os_driver osDriver;
	struct narwal_if_driver ifDriver;
	struct narwal_hwAddr ifMAC;

	getMACAddr(socketHandler, opt, ifMAC.w);

	osDriver.threadCreate      = osThreadCreate;
	osDriver.threadExit        = osThreadExit;
	osDriver.threadJoin        = osThreadJoin;

	osDriver.semaphoreCreate   = osSemaphoreCreate;
	osDriver.semaphoreTake     = osSemaphoreTake;
	osDriver.semaphoreGive     = osSemaphoreGive;
	osDriver.sempahoreGetCount = osSempahoreGetCount;
	osDriver.mutexCreate       = osMutexCreate;
	osDriver.mutexLock         = osMutexLock;
	osDriver.mutexUnlock       = osMutexUnlock;
	osDriver.getTicks          = osGetTicks;
	osDriver.freeData          = osFreeData;
	osDriver.copyMem           = osCopyMem;
	osDriver.getRandom         = osGetRandom;
	osDriver.delayMs           = osDelay;

	ifDriver.receive = NULL;
	ifDriver.send = ifSend;

	narwal_initos(osDriver);
	narwal_initif(ifDriver, ifMAC);

	keepRunning = 1;
	if(pthread_create(&recvHandler, NULL, recvThread, (void *)&keepRunning) < 0){
		printf("Error creating the receive task\n");
		return -1;
	}

	NARWAL_dhcp_start();

	while(NARWAL_dhcp_isBound() == 0)
		usleep(100000);

	char zeBuffer[512], senderStr[32];
	int socketoso, dataRecv;
	struct narwal_sockaddr_in serverAddr, clientAddr, senderAddr;
	socklen_t serverLength;

	socketoso = narwal_socket(NARWAL_AF_INET, NARWAL_SOCK_DGRAM, 0);
	if(socketoso < 0){
		printf("Bad socket\n");
		return -1;
	}

	// We
	bzero(&clientAddr, sizeof(struct narwal_sockaddr_in));
	clientAddr.sin_addr.s_addr = INADDR_ANY;
	clientAddr.sin_family = NARWAL_AF_INET;
	clientAddr.sin_port = HTOS(2988);

	// They
	bzero(&serverAddr, sizeof(struct narwal_sockaddr_in));
	serverAddr.sin_addr.s_addr = NARWAL_atoh("10.10.0.16");
	serverAddr.sin_family = NARWAL_AF_INET;
	serverAddr.sin_port = HTOS(2983);

	result = narwal_bind(socketoso, clientAddr, sizeof(struct narwal_sockaddr_in));
	if(result < 0){
		printf("Bindera bugou\n");
		return -2;
	}

	while(socketoso > 0){
		printf("Initiating connection...\n");
		strcpy(zeBuffer, "Enviando...");
		narwal_sendto(socketoso, zeBuffer, strlen(zeBuffer), 0, serverAddr, sizeof(struct narwal_sockaddr_in));

		// wait for response
		dataRecv = narwal_recvfrom(socketoso, zeBuffer, 512, 0, &senderAddr, &serverLength);
		if(dataRecv > 0){
			zeBuffer[dataRecv] = '\0';

			NARWAL_htoa(senderStr, senderAddr);
			printf("Received from \'%s\'. Message: %s\n", senderStr, zeBuffer);
		}
	}

	// terminate receiving
	keepRunning = 0;
	pthread_join(recvHandler, NULL);

	narwal_join();
	close(socketHandler);
	return 0;
}

int PrintError(char *msg, int errorCode){
	printf("Error: %s.\n\tCode: %d(%s)\n", msg, errorCode, strerror(errorCode));
	return errorCode;
}

int createRawSocket(const char *interface, int *socketFd){
	int result;

	(*socketFd) = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if((*socketFd) <= 0)
		return PrintError("Error creating RAW socket", errno);

	uint32_t optLen = strlen(interface);
	result = setsockopt((*socketFd), SOL_SOCKET, SO_BINDTODEVICE, interface, optLen);
	if(result < 0)
		return PrintError("Error selecting interface to socket", result);

	return 0;
}

int getMACAddr(int socketFd, const char *interface, uint8_t *addr){
	struct ifreq ifDetails;
	int result;

	strcpy(ifDetails.ifr_ifrn.ifrn_name, interface);
	result = ioctl(socketFd, SIOCGIFHWADDR, &ifDetails);
	if(result < 0)
		return PrintError("Error reading MAC from interface", result);

//	memset(addr, 0xAA, 6);
	memcpy(addr, ifDetails.ifr_ifru.ifru_addr.sa_data, 6);
	addr[5]++;

	result = ioctl(socketFd,SIOCGIFADDR,&ifDetails);
	if(result < 0)
		return PrintError("Error reading interface details", result);

	interfaceIndex = ifDetails.ifr_ifindex;
	return 0;
}

//static void GetIPaddr()
//	ETH_DefineMAC((uint8_t *)ifDetails.ifr_addr.sa_data);
//	if(ioctl(dhcpSock, SIOCGIFADDR, &ifDetails) < 0){
//		printf("Erro ao ler IP da if. '%s', saindo...\n", opt);
//		return -4;
//	}
//}
