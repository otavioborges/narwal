/*
 * tcp.c
 *
 *  Created on: Dec. 29, 2020
 *      Author: root
 */

#include "narwal.h"

#include "eth.h"
#include "ipv4.h"
#include "tcp.h"

void tcp_task(void){

}

void NARWAL_tcp_parsePackage(uint8_t *buffer, size_t length, struct narwal_addr sender){
	tcp_header_t *header = (tcp_header_t *)buffer;
	uint8_t *options = (buffer + NARWAL_PACKET_TCP_LENGTH);
	uint8_t *payload = (buffer + (((header->dataOffsetNS >> 4) - 5) *4)); // data offset contains header length in words (32bit)
	struct narwal_sockaddr_in addr;
	uint16_t packetFlags = ((uint16_t)(header->dataOffsetNS & 0x01) << 8) | header->flags;

	narwal_getOsDriver()->copyMem(addr.sin_addr.mac.w, sender.mac.w, NARWAL_HW_ADDR_LENGTH);
	addr.sin_addr.s_addr = sender.s_addr;
	addr.sin_port = header->sourcePort;

#ifdef NARWAL_IPv4_ENABLED
	addr.sin_family = NARWAL_AF_INET;
#else
	addr.sin_family = NARWAL_AF_INET6;
#endif

	// only push data to socket if PSH
	if(packetFlags & TCP_FLAG_PSH){
		narwal_recvForSocket(buffer, length, NARWAL_IPv4_PROTOCOL_TCP, header->destPort, &addr);
	}
}
