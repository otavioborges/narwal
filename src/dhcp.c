/*
 * dhcp.c
 *
 *  Created on: Dec. 29, 2020
 *      Author: root
 */

#define NARWAL_DHCP_CLIENT_PORT     68
#define NARWAL_DHCP_SERVER_PORT     67

#define NARWAL_DHCP_DELAY_INC		60000
#define NARWAL_DHCP_DELAY_MAX		60000

#include "dhcp.h"
#include "eth.h"
#include "arp.h"
#include "ipv4.h"

static int g_dhcpSocket;
static uint32_t g_renewalDelay = 0;
static uint32_t g_dhcpTaskDelay = NARWAL_DHCP_DELAY_INC;
static uint8_t g_dhcpState = 0;
static struct narwal_addr g_serverAddr;
static uint32_t g_currentTransaction;
static uint8_t g_dhcpBuffers[NARWAL_DHCP_BUFFER_LENGTH]; // send buffer

// DHCP periodical routines
static void dhcp_task(void);

static dhcp_option_t *dhcp_getOption(uint8_t *buffer, dhcp_option_value_t code){
	dhcp_option_t *current = (dhcp_option_t *)buffer;
	uint32_t offset = 0;

	while((current->code != DHCP_OPTION_END) && (offset < NARWAL_PACKAGES_MAX_LENGTH) && (current->code != (uint8_t )code)){
		offset += 2 + current->length;
		current = (dhcp_option_t *)(((uint8_t *)current) + 2 + current->length);
	}

	if(current->code == (uint8_t)code)
		return current;
	else
		return NULL;
}

static int dhcp_recvCallback(uint8_t *buffer,uint32_t length, struct narwal_sockaddr_in sender){
	dhcp_header_t *header = (dhcp_header_t *)buffer;
	dhcp_option_t *currentOption = NULL;
	uint8_t *payload = (buffer + NARWAL_PACKET_DHCP_LENGTH);
	uint32_t dataLength, recvNetmask, recvGateway;
	uint8_t optionsValue[8];
	struct narwal_sockaddr_in dhcpServer;

	// Parsing the received package depends on DHCP state
	if(header->opcode != DHCP_OPCODE_REPLY)
		return 0;

	// Check the MAGIC
	if(header->magicCookie != HTOL(NARWAL_DHCP_MAGIC_COOKIE))
		return 0;

	// this is not our request
	if(header->transactionID != HTOL(g_currentTransaction))
		return 0;

	// Mismatch on MAC length
	if(header->hwAddrLength != NARWAL_HW_ADDR_LENGTH)
		return 0;

	// Mismatch on destination MAC
	if(narwal_matchHwAddr(*((struct narwal_hwAddr *)(header->clientHw)), narwal_getHwAddr()) == 0)
		return 0;

	dhcpServer.sin_addr.s_addr = HTOL(NARWAL_INADDR_BROADCAST);
	dhcpServer.sin_port = HTOS(NARWAL_DHCP_SERVER_PORT);

	if(g_dhcpState & DHCP_STATE_DISCOVER){
		currentOption = dhcp_getOption(payload, DHCP_OPTION_DHCP_MSG_TYPE);
		if(!currentOption || currentOption->value != DHCP_TYPE_OFFER)
			return 0;

		// Check the received IP parameters
		currentOption = dhcp_getOption(payload, DHCP_OPTION_SUBNET_MASK);
		if(!currentOption || (currentOption->length != 4))
			return 0;
		recvNetmask = *((uint32_t *)&(currentOption->value));

		currentOption = dhcp_getOption(payload, DHCP_OPTION_ROUTER);
		if(!currentOption || (currentOption->length != 4))
			return 0;
		recvGateway = *((uint32_t *)&(currentOption->value));

		// Do we have DNS?
		currentOption = dhcp_getOption(payload, DHCP_OPTION_DOMAIN_SERVER);
		if(currentOption){
			NARWAL_ipv4_setDNSServers((uint32_t *)&(currentOption->value), (currentOption->length >> 2));
		}

		// By now all variables are set copy to the stack
		NARWAL_ipv4_setIP(header->yourAddr);
		NARWAL_ipv4_setNetmask(recvNetmask);
		NARWAL_ipv4_setGateway(recvGateway);

		// form the reply
		dataLength = NARWAL_dhcp_BuildMessage(g_dhcpBuffers, DHCP_OPCODE_REQUEST, g_currentTransaction, 1);

		// Add default options
		optionsValue[0] = DHCP_TYPE_REQUEST;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_DHCP_MSG_TYPE, 1, optionsValue);

		*(uint16_t *)optionsValue = NARWAL_DHCP_BUFFER_LENGTH;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_DHCP_MAX_MSG_SIZE, 2, optionsValue);

		// What options we require
		optionsValue[0] = DHCP_OPTION_SUBNET_MASK;
		optionsValue[1] = DHCP_OPTION_ROUTER;
		optionsValue[2] = DHCP_OPTION_DOMAIN_SERVER;
		optionsValue[3] = DHCP_OPTION_BROADCAST_ADDRESS;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_PARAMETER_LIST, 4, optionsValue);

		if(narwal_getHostname()){
			dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_HOSTNAME, narwal_getHostnameLength(), narwal_getHostname());
		}

		if(g_dhcpSocket >= 0){
			if(dataLength > 0){
				dataLength = narwal_sendto(g_dhcpSocket, g_dhcpBuffers, dataLength, NARWAL_SOCKET_FLAG_NOWAIT, dhcpServer, 0);
				if(dataLength > 0) // Data was sent
					g_dhcpState = DHCP_STATE_OFFER;
			}
		}

		return dataLength;
	}else if(g_dhcpState & (DHCP_STATE_OFFER | DHCP_STATE_RENEWING)){
		currentOption = dhcp_getOption(payload, DHCP_OPTION_DHCP_MSG_TYPE);
		if(!currentOption || currentOption->value != DHCP_TYPE_ACK){
			// Invalidate the Discover values and return to discovering
			NARWAL_ipv4_setIP(0);
			NARWAL_ipv4_setNetmask(0);
			NARWAL_ipv4_setGateway(0);

			// Erase server details
			narwal_copyHwAddr(&(g_serverAddr.mac), NARWAL_ETH_BROADCAST);
			g_serverAddr.s_addr = NARWAL_INADDR_BROADCAST;

			g_dhcpState = 0;
		}else{
			// Look at that marvelous IP, keep it, treasure it......until renewal
			narwal_copyHwAddr(&(g_serverAddr.mac), sender.sin_addr.mac);
			g_serverAddr.s_addr = sender.sin_addr.s_addr;

			currentOption = dhcp_getOption(payload, DHCP_OPTION_RENEWAL_TIME);
			if(!currentOption || (currentOption->length != 4))
				g_renewalDelay = NARWAL_DHCP_DEFAULT_RENEWAL * 1000;
			else
				g_renewalDelay = (HTOL(*((uint32_t *)&(currentOption->value)))) * 1000;

			g_dhcpState |= DHCP_STATE_BOUND;
			g_dhcpState &= ~(DHCP_STATE_OFFER | DHCP_STATE_RENEWING);
			narwal_postTask(dhcp_task, g_renewalDelay);
		}
	}

	// Not a valid state to receive crap
	return 0;
}

void dhcp_task(void){
	struct narwal_os_driver *driver = narwal_getOsDriver();
	uint8_t optionsValue[16];
	uint32_t dataLength;
	struct narwal_sockaddr_in sendAddr;

	if(g_dhcpState & DHCP_STATE_BOUND){
		// TIIIIIIME for renewal, morph
		g_currentTransaction = driver->getRandom();
		dataLength = NARWAL_dhcp_BuildMessage(g_dhcpBuffers, DHCP_OPCODE_REQUEST, g_currentTransaction, 1);

		// Add default options
		optionsValue[0] = DHCP_TYPE_REQUEST;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_DHCP_MSG_TYPE, 1, optionsValue);

		*(uint16_t *)optionsValue = NARWAL_DHCP_BUFFER_LENGTH;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_DHCP_MAX_MSG_SIZE, 2, optionsValue);

		// What options we require
		optionsValue[0] = DHCP_OPTION_SUBNET_MASK;
		optionsValue[1] = DHCP_OPTION_ROUTER;
		optionsValue[2] = DHCP_OPTION_DOMAIN_SERVER;
		optionsValue[3] = DHCP_OPTION_BROADCAST_ADDRESS;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_PARAMETER_LIST, 4, optionsValue);

		sendAddr.sin_addr.s_addr = HTOL(NARWAL_INADDR_BROADCAST);
		sendAddr.sin_port = HTOS(NARWAL_DHCP_SERVER_PORT);

		if(g_dhcpSocket >= 0){
			if(dataLength > 0){
				dataLength = narwal_sendto(g_dhcpSocket, g_dhcpBuffers, dataLength, NARWAL_SOCKET_FLAG_NOWAIT, sendAddr, 0);
				if(dataLength > 0) // Data was sent
					g_dhcpState |= DHCP_STATE_RENEWING;
			}
		}
	}else{
		// THIS IS THE BEGINNIIIIIIIIIIIIIIIIIING
		g_currentTransaction = driver->getRandom();
		dataLength = NARWAL_dhcp_BuildMessage(g_dhcpBuffers, DHCP_OPCODE_REQUEST, g_currentTransaction, 1);

		// Add default options
		optionsValue[0] = DHCP_TYPE_DISCOVER;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_DHCP_MSG_TYPE, 1, optionsValue);

		*(uint16_t *)optionsValue = NARWAL_DHCP_BUFFER_LENGTH;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_DHCP_MAX_MSG_SIZE, 2, optionsValue);

		// What options we require
		optionsValue[0] = DHCP_OPTION_SUBNET_MASK;
		optionsValue[1] = DHCP_OPTION_ROUTER;
		optionsValue[2] = DHCP_OPTION_DOMAIN_SERVER;
		optionsValue[3] = DHCP_OPTION_BROADCAST_ADDRESS;
		dataLength = NARWAL_dhcp_AddOption(g_dhcpBuffers, DHCP_OPTION_PARAMETER_LIST, 4, optionsValue);

		sendAddr.sin_addr.s_addr = HTOL(NARWAL_INADDR_BROADCAST);
		sendAddr.sin_port = HTOS(NARWAL_DHCP_SERVER_PORT);

		if(g_dhcpSocket >= 0){
			if(dataLength > 0){
				dataLength = narwal_sendto(g_dhcpSocket, g_dhcpBuffers, dataLength, NARWAL_SOCKET_FLAG_NOWAIT, sendAddr, 0);
				if(dataLength > 0) // Data was sent
					g_dhcpState = DHCP_STATE_DISCOVER;
			}
		}
	}

	// dont update the delay when bound
	if((g_dhcpState & DHCP_STATE_BOUND) == 0){
		if(g_dhcpTaskDelay < NARWAL_DHCP_DELAY_MAX)
			g_dhcpTaskDelay += NARWAL_DHCP_DELAY_INC;

		narwal_postTask(dhcp_task, g_dhcpTaskDelay);
	}
}

int NARWAL_dhcp_start(void){
	struct narwal_sockaddr_in dhcpAddr;
	int result;

	// Clear server addr for now
	g_serverAddr.s_addr = HTOL(NARWAL_INADDR_ANY);

	// Create a socket to listen to DHCP packets
	g_dhcpSocket = narwal_socket(NARWAL_AF_INET, NARWAL_SOCK_DGRAM, 0);
	if(g_dhcpSocket < 0)
		return g_dhcpSocket;

	dhcpAddr.sin_addr.s_addr = HTOL(NARWAL_INADDR_BROADCAST);
	dhcpAddr.sin_family = NARWAL_AF_INET;
	dhcpAddr.sin_port = HTOS(NARWAL_DHCP_CLIENT_PORT);

	result = narwal_bind(g_dhcpSocket, dhcpAddr, 0);
	if(result < 0){
		narwal_close(g_dhcpSocket);
		return result;
	}

	narwal_socketCallback(g_dhcpSocket, dhcp_recvCallback);

	// publish the DHCP task
//	narwal_postTask(dhcp_task, g_dhcpTaskDelay);
	dhcp_task();

	return 0;
}

int NARWAL_dhcp_BuildMessage(uint8_t *buffer, dhcp_opcode_t opcode, uint32_t transactionID, uint8_t broadcast){
	dhcp_header_t *header = (dhcp_header_t *)buffer;
	dhcp_option_t *options = (dhcp_option_t *)(buffer + NARWAL_PACKET_DHCP_LENGTH);

	header->opcode        = (uint8_t)opcode;
	// We only operate with normal MAC
	header->hwType        = NARWAL_HWTYPE_ETHERNET;
	header->hwAddrLength  = 6;

	header->hopCount      = 0;
	header->transactionID = HTOL(transactionID);
	header->seconds       = 0xFFFFU;	// bullcrap, leave it

	if(broadcast)
		header->flags     = 0x8000U;	// Set broadcast flag
	else
		header->flags     = 0;

	header->clientAddr    = NARWAL_ipv4_ourAddr();
	header->yourAddr      = 0;
	header->serverAddr    = 0;
	header->gatewayAddr   = 0;
	narwal_copyHwAddr((struct narwal_hwAddr *)(header->clientHw), narwal_getHwAddr());

	// Zero server name and boot option
	for(int idx = 0; idx < 128; idx++){
		if(idx < 64)
			header->serverName[idx] = 0;

		header->boot[idx]           = 0;
	}

	header->magicCookie   = HTOL(NARWAL_DHCP_MAGIC_COOKIE);

	options->code = DHCP_OPTION_END;
	return (NARWAL_PACKET_DHCP_LENGTH + 1);
}

int NARWAL_dhcp_AddOption(uint8_t *buffer, dhcp_option_value_t option, uint8_t optLength, uint8_t *value){
	dhcp_option_t *options = (dhcp_option_t *)(buffer + NARWAL_PACKET_DHCP_LENGTH);
	uint8_t *optionsValue = 0;
	uint32_t offset = 0;

	while((options->code != DHCP_OPTION_END) && (offset < NARWAL_PACKAGES_MAX_LENGTH)){
		offset += 2 + options->length;
		options = (dhcp_option_t *)(((uint8_t *)options) + 2 + options->length);
	}

	if(options->code != DHCP_OPTION_END)
		return -1;	// incorrect format

	offset += 2 + optLength;
	options->code = (uint8_t)option;
	options->length = optLength;

	optionsValue = &(options->value);
	for(uint32_t optIdx = 0; optIdx < options->length; optIdx++)
		optionsValue[optIdx] = value[optIdx];

	// Reinsert END option at the end
	options = (dhcp_option_t *)(((uint8_t *)options) + 2 + options->length);
	options->code = DHCP_OPTION_END;

	offset++;
	return (NARWAL_PACKET_DHCP_LENGTH + offset);
}

int NARWAL_dhcp_isBound(void){
	if(g_dhcpState & DHCP_STATE_BOUND)
		return 1;
	else
		return 0;
}
