/*
 * ipv4.c
 *
 *  Created on: Dec. 28, 2020
 *      Author: root
 */

#define FAST_10_DIV(x)         (uint8_t)((((uint32_t)x) * 52U) >> 9)

#include "ipv4.h"

#include "tcp.h"
#include "udp.h"

static uint32_t g_ourIP = 0, g_netmask = 0, g_gateway = 0;
static uint32_t g_dnsServers[NARWAL_DNS_SERVER_COUNT];

static void ipv4_headerChecksum(ipv4_header_t *header){
	uint16_t *data = (uint16_t *)header;
	uint32_t result = 0;
	header->checksum = 0;

	for(uint8_t idx = 0; idx < (NARWAL_PACKET_IPv4_LENGTH >> 1); idx++){
		result += data[idx];
	}

	result = (result & 0xFFFF) + ((result >> 16) & 0xFFFF);
	header->checksum = ~((uint16_t)result);
}

uint32_t NARWAL_atoh(char *addr){
	uint32_t result = 0, idx = 0;
	uint8_t octet = 0;
	uint8_t offset = 0;

	while(addr[idx] != '\0'){
		if(addr[idx] == '.'){
			if(offset >= 24) // do many dots
				return 0;

			result |= (octet << offset);
			octet = 0;
			offset += 8;
		}else if((addr[idx] >= '0') && (addr[idx] <= '9')){
			octet *= 10;
			octet += (addr[idx] - '0');
		}else{
			// Invalid char
			return 0;
		}

		idx++;
	}

	result += octet << 24; // add last part
	return result;
}

void NARWAL_htoa(char *__s, struct narwal_sockaddr_in __a){
	uint32_t ipValue = __a.sin_addr.s_addr;
	uint8_t octet = ipValue & 0xFF;
	uint16_t ipIdx = 0;

	for(uint8_t oc = 0; oc < 4; oc++){
		if(octet == 0){
			__s[ipIdx++] = '0';
		}else if(octet < 9U){
			__s[ipIdx++] = '0' + octet;
		}else if(octet < 99U){
			__s[ipIdx++] = '0' + FAST_10_DIV(octet);
			__s[ipIdx++] = '0' + (octet - (FAST_10_DIV(octet) * 10));
		}else{
			__s[ipIdx++] = '0' + FAST_10_DIV(FAST_10_DIV(octet));

			octet -= (FAST_10_DIV(FAST_10_DIV(octet)) * 100);
			__s[ipIdx++] = '0' + FAST_10_DIV(octet);

			octet -= FAST_10_DIV(octet) * 10;
			__s[ipIdx++] = '0' + octet;
		}

		if(oc < 3)
			__s[ipIdx++] = '.';
	}

	__s[ipIdx] = '\0';
}

void NARWAL_ipv4_setIP(uint32_t addr){
	g_ourIP = addr;
}

void NARWAL_ipv4_setNetmask(uint32_t addr){
	g_netmask = addr;
}

void NARWAL_ipv4_setGateway(uint32_t addr){
	g_gateway = addr;
}

void NARWAL_ipv4_setDNSServers(uint32_t *addrs, uint32_t length){
	if(length > NARWAL_DNS_SERVER_COUNT)
		length = NARWAL_DNS_SERVER_COUNT;

	for(uint32_t idx = 0; idx < length; idx++)
		g_dnsServers[idx] = addrs[idx];
}

uint32_t NARWAL_ipv4_getBroadcast(void){
	return (~g_netmask | g_ourIP);
}

uint32_t NARWAL_ipv4_getGateway(void){
	return g_gateway;
}

uint32_t NARWAL_ipv4_ourAddr(void){
	return g_ourIP;
}

int NARWAL_ipv4_matchAddr(uint32_t a, uint32_t b){
	return (a == b);
}

void NARWAL_ipv4_parsePackage(uint8_t *buffer, size_t length, struct narwal_addr sender){
	ipv4_header_t *header = (ipv4_header_t *)buffer;
	uint8_t *payload;

	if((header->versionIHL & 0xF0) != NARWAL_IPv4_VERSION) // this is not IPv4
		return;

	// Use IHL to calculate data offset
	if((header->versionIHL & 0x0F) < 5) // IHL has to be greater than 5
		return;

	payload = (buffer + ((header->versionIHL & 0x0F) << 2));
	sender.s_addr = header->sourceIP;

	// add that to ARP table
	narwal_addArpEntry(sender);

	// If we have an IP, check if the package is for us or is a broadcast
	if(g_ourIP){
		if(header->destIP != g_ourIP){
			// Might be a broadcast
			if(header->destIP != NARWAL_ipv4_getBroadcast())
				return;	// NOT us
		}
	}

	// Protocol switch case
	switch(header->protocol){
	case NARWAL_IPv4_PROTOCOL_UDP:
		NARWAL_udp_parsePackage(payload, (HTOS(header->totalLength) - ((header->versionIHL & 0x0F) << 2)), sender);
		break;
	case NARWAL_IPv4_PROTOCOL_TCP:
		NARWAL_tcp_parsePackage(payload, (HTOS(header->totalLength) - ((header->versionIHL & 0x0F) << 2)), sender);
		break;
	}
}

int NARWAL_ipv4_createHeader(uint8_t *buffer, struct narwal_addr who, uint8_t *payload, uint32_t payloadLength, uint32_t ID, uint8_t protocol){
	ipv4_header_t *header = (ipv4_header_t *)buffer;

	header->versionIHL = (NARWAL_IPv4_VERSION | 5);
	header->dscpECN = 0;
	header->totalLength = HTOS((payloadLength + NARWAL_PACKET_IPv4_LENGTH));
	header->ID = ID;
	header->fragmentOffset = HTOS(0x4000);
	header->ttl = 64;
	header->protocol = protocol;
	header->checksum = 0;
	header->sourceIP = NARWAL_ipv4_ourAddr();
	header->destIP = who.s_addr;

	ipv4_headerChecksum(header);

	if(payload){
		narwal_getOsDriver()->copyMem((buffer + NARWAL_PACKET_IPv4_LENGTH), payload, payloadLength);
		return (NARWAL_PACKET_IPv4_LENGTH + payloadLength);
	}else{
		return NARWAL_PACKET_IPv4_LENGTH;
	}
}
