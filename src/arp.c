/*
 * arp.c
 *
 *  Created on: Dec. 28, 2020
 *      Author: root
 */

#include "arp.h"
#include "eth.h"

#ifdef NARWAL_IPv4_ENABLED
#include "ipv4.h"
#else
#include "ipv6.h"
#endif

static void arp_request(arp_header_t *header, uint8_t *payload, struct narwal_addr sender){
	uint32_t packetLength;
	arp_header_t *respHeader;
	uint8_t *response, *respPayload;

	if(HTOS(header->hwType) != NARWAL_HWTYPE_ETHERNET) // We don't implement other HW types, who uses that? pf
		return;

	if(header->hwAddrLength != NARWAL_HW_ADDR_LENGTH) // Requester is asking for a hw address that we don't have
		return;

#ifdef NARWAL_IPv4_ENABLED
	if(header->protoAddrLength != 4)                  // We are operating with IPv4
		return;
#else
	if(header->protoAddrLength != 16)                 // We are operating with IPv6
			return;
#endif

	// Check if they are asking for us
	if(!NARWAL_ipv4_matchAddr(*(uint32_t *)(payload + NARWAL_ARP_ENTRY_LENGTH + NARWAL_HW_ADDR_LENGTH), NARWAL_ipv4_ourAddr())){
		// that's not us, fuck it!
		return;
	}

	response = narwal_getNextSendBuffer();
	if(!response)
		return; //TODO: fix a wait when send buffer are full

	packetLength = NARWAL_eth_createHeader(response, sender.mac.w, narwal_getHwAddr().w, NARWAL_ETHERTYPE_ARP);
	respHeader = (arp_header_t *)(response + packetLength);
	respPayload = (response + packetLength + NARWAL_PACKET_ARP_LENGTH);

	packetLength += NARWAL_PACKET_ARP_LENGTH;
	respHeader->hwType = HTOS(NARWAL_HWTYPE_ETHERNET);
	respHeader->hwAddrLength = 6;

#ifdef NARWAL_IPv4_ENABLED
	respHeader->protoType = HTOS(NARWAL_ETHERTYPE_IPv4);
	respHeader->protoAddrLength = 4;
#else
	respHeader->protoType = HTOS(NARWAL_ETHERTYPE_IPv6);
	respHeader->protoAddrLength = 16;
#endif

	packetLength += 2 * NARWAL_ARP_ENTRY_LENGTH;
#ifdef NARWAL_IPv4_ENABLED
	narwal_copyHwAddr((struct narwal_hwAddr *)respPayload, narwal_getHwAddr());
	*((uint32_t *)(respPayload + NARWAL_HW_ADDR_LENGTH)) = NARWAL_ipv4_ourAddr();

	narwal_copyHwAddr((struct narwal_hwAddr *)(respPayload + NARWAL_ARP_ENTRY_LENGTH), sender.mac);
	*((uint32_t *)(respPayload + NARWAL_HW_ADDR_LENGTH + NARWAL_ARP_ENTRY_LENGTH)) = (*(uint32_t *)(payload + NARWAL_ARP_ENTRY_LENGTH));
#else
	// TODO: We need to copy ipv6s
	narwal_copyHwAddr(respPayload, narwal_getHwAddr());
//	*((uint32_t *)(respPayload + NARWAL_HW_ADDR_LENGTH)) = HTOL(NARWAL_ipv6_ourAddr());

	narwal_copyHwAddr((respPayload + NARWAL_ARP_ENTRY_LENGTH), sender.mac);
//	*((uint32_t *)(respPayload + NARWAL_HW_ADDR_LENGTH + NARWAL_ARP_ENTRY_LENGTH)) = HTOL((*(uint32_t *)(payload + NARWAL_ARP_ENTRY_LENGTH)));
#endif

	narwal_signalSendPackage(response, packetLength);
}

static void arp_reply(arp_header_t *header, uint8_t *payload){
	int packetLength = 0;
	arp_header_t *respHeader;
	uint8_t *respPayload;
	struct narwal_addr arpSender;


	if(HTOS(header->hwType) != NARWAL_HWTYPE_ETHERNET) // We don't implement other HW types, who uses that? pf
		return;

	if(header->hwAddrLength != NARWAL_HW_ADDR_LENGTH) // Requester is asking for a hw address that we don't have
		return;

#ifdef NARWAL_IPv4_ENABLED
	if(header->protoAddrLength != 4)                  // We are operating with IPv4
		return;
#else
	if(header->protoAddrLength != 16)                 // We are operating with IPv6
			return;
#endif

	// Check if response is for us
	if(narwal_matchHwAddr(*(struct narwal_hwAddr *)(payload + NARWAL_ARP_ENTRY_LENGTH), narwal_getHwAddr())){
		narwal_copyHwAddr(&(arpSender.mac), *(struct narwal_hwAddr *)(payload));
		arpSender.s_addr = *((uint32_t *)(payload + NARWAL_HW_ADDR_LENGTH));

		narwal_addArpEntry(arpSender);
	}
}

void NARWAL_arp_parsePackage(uint8_t *buffer, size_t length, struct narwal_addr sender){
	arp_header_t *header = (arp_header_t *)buffer;
	uint8_t *payload = (buffer + NARWAL_PACKET_ARP_LENGTH);

	switch(HTOS(header->opcode)){
	case NARWAL_OPCODE_ARP_REQUEST:
		arp_request(header, payload, sender);
		break;
	case NARWAL_OPCODE_ARP_REPLY:
		arp_reply(header, payload);
		break;
	}
}

int NARWAL_arp_createPackage(struct narwal_addr who, uint8_t *payload){
	int packetLength = 0;
	arp_header_t *respHeader;
	uint8_t *respPayload;

	packetLength = NARWAL_eth_createHeader(payload, NARWAL_ETH_BROADCAST.w, narwal_getHwAddr().w, NARWAL_ETHERTYPE_ARP);
	respHeader = (arp_header_t *)(payload + packetLength);
	respPayload = (payload + packetLength + NARWAL_PACKET_ARP_LENGTH);

	packetLength += NARWAL_PACKET_ARP_LENGTH;
	respHeader->hwType = HTOS(NARWAL_HWTYPE_ETHERNET);
	respHeader->hwAddrLength = 6;
	respHeader->opcode = HTOS(NARWAL_OPCODE_ARP_REQUEST);

#ifdef NARWAL_IPv4_ENABLED
	respHeader->protoType = HTOS(NARWAL_ETHERTYPE_IPv4);
	respHeader->protoAddrLength = 4;
#else
	respHeader->protoType = HTOS(NARWAL_ETHERTYPE_IPv6);
	respHeader->protoAddrLength = 16;
#endif

	packetLength += 2 * NARWAL_ARP_ENTRY_LENGTH;
#ifdef NARWAL_IPv4_ENABLED
	narwal_copyHwAddr((struct narwal_hwAddr *)respPayload, narwal_getHwAddr());
	*((uint32_t *)(respPayload + NARWAL_HW_ADDR_LENGTH)) = NARWAL_ipv4_ourAddr();


	narwal_copyHwAddr((struct narwal_hwAddr *)(respPayload + NARWAL_ARP_ENTRY_LENGTH), NARWAL_ETH_NONE);
	*((uint32_t *)(respPayload + NARWAL_HW_ADDR_LENGTH + NARWAL_ARP_ENTRY_LENGTH)) = who.s_addr;
#else
	// TODO: We need to copy ipv6s
	narwal_copyHwAddr(respPayload, narwal_getHwAddr());
//	*((uint32_t *)(respPayload + NARWAL_HW_ADDR_LENGTH)) = HTOL(NARWAL_ipv6_ourAddr());

	narwal_copyHwAddr((respPayload + NARWAL_ARP_ENTRY_LENGTH), sender.mac);
//	*((uint32_t *)(respPayload + NARWAL_HW_ADDR_LENGTH + NARWAL_ARP_ENTRY_LENGTH)) = HTOL((*(uint32_t *)(payload + NARWAL_ARP_ENTRY_LENGTH)));
#endif

	return packetLength;
}
